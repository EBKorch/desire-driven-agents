extern crate rayon;
use rayon::prelude::*;

use geom2d::{
    self,
    Point,
    Vector,
};

use crate::objs::*;
use crate::idmap::*;

use std::sync::{mpsc, Mutex, Arc};
use std::thread;

const THREAD_N: usize = 4;

pub struct Universe {
    objs: IDMap<Object>,
    pool: rayon::ThreadPool,
}

impl Universe{
    pub fn new() -> Self {
        Universe {
            objs: IDMap::new(),
            pool: rayon::ThreadPoolBuilder::new().num_threads(4).build().unwrap(),
        }
    }

    pub fn iter(&self) -> std::collections::hash_map::Iter<'_, usize, Object>{
        self.objs.get_all().iter()
    }

    pub fn get(&self, id: &usize) -> Option<&Object> {
        self.objs.get(id)
    }

    pub fn get_all_key(&self) -> Vec<&usize> {
        self.objs.keys()
    }

    pub fn add_object(&mut self, obj: Object) -> Option<usize> {
        if let Some(key) = self.objs.insert(obj) {
            Some(key)
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.objs.len()
    }

    pub fn step(&mut self, dt: f64) -> Vec<usize> {
        let removed: Arc<Mutex<Vec<usize>>> = Arc::new(Mutex::new(Vec::new()));
        let all_item = Arc::new(self.objs.get_all().clone());
        let mut all_item_vec: Vec<(&usize, &mut Object)> = self.objs.get_all_mut().iter_mut().collect();
        let chunk_size = (all_item.len() as f64/THREAD_N as f64).ceil() as usize;
        if chunk_size == 0 {
            return Vec::new()
        }
        self.pool.scope(|s| {
            for chunk in &mut all_item_vec.chunks_mut(chunk_size) {
                let removed_clone = removed.clone();
                let all_item_clone = all_item.clone();
                s.spawn(move |_| {
                    for (_, item) in chunk {
                        match item {
                            Object::OAgent(agent) => {
                                agent.step(dt, &all_item_clone, removed_clone.clone());
                                agent.pos.up(move_and_slide(agent, &all_item_clone, dt));
                            }
                            Object::OItem(_) => {

                            }
                            Object::OWall(_) => {

                            }
                        }
                    }
                });
            }
        });
        // Remove deleted items
        let removed = &*removed.lock().unwrap();
        for id in removed {
            self.objs.remove(id);
        }
        
        removed.clone()
        //println!("Item count: {}", self.objects.len());
    }

    /// Adds an agent defined by the starting point and
    /// with zero initial velocity.
    pub fn add_agent(&mut self, pos: Point) -> Option<usize> {
        let mut agent = Agent::new(pos, Vector::new(20,20));
        let desire_explore = Desire::new("Explore",
            vec![
                Reaction::new_linear("BaseUrge", (0.0,0.0), 1.0)
            ]
        );
        let desire_eat = Desire::new("Eat",
            vec![
                Reaction::new_linear("Hunger", (0.0, 1.0), 1.0)
            ],
        );
        agent.desires = vec![desire_explore, desire_eat];
        agent.bloodstream.insert(
            String::from("Hunger"),
            Hormon::new(HFunc::Linear(1.0), vec![PropFlag::Food(1.0)]),
        );
        self.add_object(Object::OAgent(agent))
    }

    /// Adds an agent with custom properties
    pub fn add_custom_agent(&mut self, pos: Point,
                            desires: Vec<Desire>, hormons: Vec<(String, Hormon)>) {
        let mut agent = Agent::new(pos, Vector::new(0,0));
        // Append desires
        for desire in desires {
            agent.desires.push(desire);
        }
        // Append hormons
        for (name, hormon) in hormons {
            agent.bloodstream.insert(name, hormon);
        }
        // Add new agent to universe
        self.add_object(Object::OAgent(agent));
    }

    /// Adds a collider to the universe
    pub fn add_wall(&mut self, pos: Point, points: Vec<Point>) {
        self.add_object(
            Object::OWall(
                Wall{pos, points}
            )
        );
    }

    /// Adds an item with the flag specified to the world
    pub fn add_item(&mut self, pos: Point, flags: Vec<PropFlag>) -> Option<usize>{
        self.add_object(
            Object::OItem(
                Item::new(pos, flags)
            )
        )
    }
}

impl Default for Universe {
    fn default() -> Self {
        Universe {
            objs: IDMap::new(),
            pool: rayon::ThreadPoolBuilder::new().num_threads(4).build().unwrap()
        }
    }
}