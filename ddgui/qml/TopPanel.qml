import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12

// Small top panel containing buttons
//
// When there is more element in the panel than it can show, a scroll
// bar appears to reveal overflowing items. The panel can also be
// closed. Some buttons work as tabs, and are mutually exclusive (Add,
// Inspector, etc) as these control the side panel.
Item {
    id: root

    // Signals which will be triggered by the inner buttons
    signal hideClicked
    signal darkModeClicked
    signal addClicked
    signal stepClicked
    signal inspectorClicked
    // As this button is "sticky" we also would  like to get the
    // state information
    signal autoStepClicked(bool checked)

    // A helper function for external control of tab selection
    function select(name) {
        if (name == "addbutton") addbutton.selected = true;
        else if (name == "addagent") addagent.selected = true;
        else if (name == "addfood") addfood.selected = true;
        else if (name == "inspector") inspector.selected = true;
    }

    // Helper function to unselect all tab buttons
    function unselect_all() {
        addbutton.selected = false;
        addagent.selected = false;
        addfood.selected = false;
        inspector.selected = false;
    }

    // Unique background for the hiding button. This is required to
    // avoid visual glitches.
    Pane {
        anchors.fill: hideitem
        anchors.leftMargin: -1
        Material.elevation: 1
    }

    // The background of the whole panel
    Pane {
        id: bgrpane
        anchors.fill: parent
        Material.elevation: 1
    }

    // A flickable area containing the full content
    Flickable {
        id: buttonflick
        anchors {
            top: parent.top
            left: hideitem.right
            right: parent.right
        }
        height: parent.height
        // Make it a bit wider than the content wide which will result
        // in a small padding added to the end
        contentWidth: buttonrow.width + 5
        flickableDirection: Flickable.HorizontalFlick
        boundsBehavior: Flickable.DragOverBounds

        // Show items in a single row
        RowLayout {
            id: buttonrow
            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }

            // The tab buttons use the same styling, only the text and icon,
            // and called signals are different

            FlatButton {
                id: addbutton
                text: "Add"
                Layout.preferredHeight: parent.height
                iconSource: "icon/plus"
                onClicked: root.addClicked()
                selected: true
            }

            FlatButton {
                id: addagent
                text: "Add agent"
                Layout.preferredHeight: parent.height
                iconSource: "icon/cpu"
            }

            FlatButton {
                id: addfood
                text: "Add food"
                Layout.preferredHeight: parent.height
                iconSource: "icon/pizza"
            }

            FlatButton {
                id: inspector
                text: "Inspector"
                Layout.preferredHeight: parent.height
                iconSource: "icon/cpu"
                onClicked: root.inspectorClicked()
            }

            // A button for manual stepping of the simulation
            FlatButton {
                text: "Step world"
                Layout.preferredHeight: parent.height
                iconSource: "icon/chevron-right"
                onClicked: root.stepClicked()
            }

            // Switches between light and dark modes
            FlatButton {
                text: "Dark mode"
                Layout.preferredHeight: parent.height
                onClicked: {
                    text = (text == "Dark mode" ? "Light mode" : "Dark mode")
                    root.darkModeClicked()
                }
            }

            // Checkbox for automatic stepping (aka. continues simulation)
            CheckBox {
                font.capitalization: Font.AllUppercase
                text: "Auto step"
                Layout.preferredHeight: parent.height
                onClicked: root.autoStepClicked(checked)
            }
        }
    }

    // Add a gradient to the edge when the button row overflows of the panel,
    // and can be scrolled. This is a visual feedback to remind the user that
    // more buttons are available.
    Rectangle {
        anchors {
            top: root.top
            left: root.left
        }
        color: Material.background
        width: hideitem.width
        height: parent.height
        anchors.leftMargin: -1
        // Using a linear gradient for inner shadow effect
        LinearGradient {
            anchors {
                top: parent.top
                left: parent.right
                bottom: parent.bottom
            }
            height: parent.height
            width: 5
            start: Qt.point(0, 0)
            end: Qt.point(width, 0)
            gradient: Gradient {
                GradientStop {position: 0.1; color: "#30000000"}
                GradientStop {position: 0.5; color: "#10000000"}
                GradientStop {position: 1.0; color: "#00000000"}
            }
            opacity: Math.min(buttonflick.contentX/10, 1)
        }
    }

    // Button with which the panel can be hided
    Item {
        id: hideitem
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
        }
        // Try to make it rectengular, but if it would be too wide,
        // constrain it to a maximum of 30 px
        width: Math.min(parent.height, 30)
        height: 15
        // Use a single image (up or down arrow), but only use the raw
        // image as a mask to the ColorOverlay to be able to change
        // the color to white when dark mode is selected.
        Image {
            id: hideicon
            anchors.fill: parent
            source: "qrc:///icon/chevron-up"
            fillMode: Image.PreserveAspectFit
            sourceSize.width: width
            sourceSize.height: height
            visible: false
        }
        ColorOverlay {
            anchors.fill: hideicon
            source: hideicon
            color: Material.foreground
            antialiasing: true
        }
    }

    // Catches click events on the hideing button
    MouseArea {
        anchors {
            top: parent.top
            left: parent.left
            bottom: hideitem.bottom
            right: hideitem.right
        }
        onClicked: {
            root.hideClicked();
            root.state = (root.state == "closed" ? "" : "closed");
        }
    }

    // Two states are defined: the default one and "closed" state. In case
    // of the latter, the hide button icon os rotated to make it a down arrow
    // and two anchors are changed: the top anchor will become the bottom one,
    // and the top becomes undefined. This, combined with an anchor animation,
    // makes a nice sliding-out effect.
    // Some extra tweaking is also required.
    states: [
        State {
            name: "closed"
            AnchorChanges {target: root; anchors{bottom: parent.top; top: undefined}}
            PropertyChanges {target: root; anchors.bottomMargin: 2}
            AnchorChanges {target: hideitem; anchors{verticalCenter: undefined; top: parent.bottom}}
            PropertyChanges {target: hideicon; rotation: 180}
        }
    ]

    transitions: [
        Transition {
            to: "*"
            AnchorAnimation {}
            PropertyAnimation {target: hideicon}
            PropertyAnimation {target: hideitem}
            PropertyAnimation {target: root}
            RotationAnimation {}
        }
    ]
}
