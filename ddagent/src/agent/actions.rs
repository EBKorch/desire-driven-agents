use super::{
    requirement::Requirement,
    effect::{ Effect, Change, DynamicChange },
    item::Item,
};

use std::iter::FromIterator;

use std::collections::HashMap;

#[derive(Clone)]
pub struct Action {
    pub name: String,
    pub data: ActionData,
}

impl Action {
    pub fn new(name: String, data: ActionData) -> Self {
        Action { name, data }
    }
} 

impl FromIterator<Action> for HashMap<String, ActionData> {
    fn from_iter<T: IntoIterator<Item = Action>>(iter: T) -> HashMap<String, ActionData> {
        let mut hashmap = HashMap::new();
        for i in iter {
            hashmap.insert(i.name, i.data);
        }
        hashmap
    }
}

#[derive(Clone)]
pub struct ActionData {
    pub requirements: Vec<Requirement>,
    pub effects: Vec<Effect>,
    pub duration: Option<f64>,
    pub has_target: bool,
    pub modifier_count: u32,
}

impl ActionData {
    pub fn new(requirements: Vec<Requirement>, effects: Vec<Effect>, duration: Option<f64>,
        has_target: bool, modifier_count: u32) -> Self {
        ActionData {
            requirements,
            effects,
            duration,
            has_target,
            modifier_count
        }
    }

    pub fn get_effect_with_target(&self, target: &Item, selfitem: &Item) -> Vec<Effect> {
        let mut effects = Vec::new();
        for effect in &self.effects {
            if let Change::Dynamic(dynamic_change) = &effect.change {
                match dynamic_change {
                    DynamicChange::Position(factor) if self.has_target => {
                        if target.state.is_none() || selfitem.state.is_none() { continue }
                        let pos_target = target.state.as_ref().unwrap().position;
                        let pos_self = selfitem.state.as_ref().unwrap().position;
                        println!("Dynamic {}", factor);
                        effects.push(
                            Effect::new(
                                effect.hormon_name.clone(),
                                Change::Absolute(factor*(pos_target-pos_self).length())
                            )
                        )
                    }
                    _ => (),
                }
            } else {
                effects.push(effect.clone());
            }
        }
        effects
    }
}