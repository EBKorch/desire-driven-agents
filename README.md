# Todo

High priority targets:

- [ ] Finish the GTK gui implementation

- [ ] Implement subdesire mechanism

- [ ] Implement proper pathfinding algorithm

- [ ] Dynamically populated and adapted knowledgebase

- [ ] Add more senses

- [ ] Create python wrapper