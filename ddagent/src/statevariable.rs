use std::ops::{
    Add, Mul, Sub, Div,
};

#[derive(Copy,Clone)]
pub struct StateVar<T> where
    T: Copy + Sub,
    <T as Sub>::Output: Div<f64> {
    v0: Option<T>,
    v1: Option<T>,
    v2: Option<T>,
    v3: Option<T>,
}

/// Implementation of StateVar. All function listed here
/// are available for all StateVar type.
impl<T> StateVar<T> where
    T: Copy + Sub,
    <T as Sub>::Output: Div<f64> {
    /// Create a new instance of the class. Using this
    /// function is unavoidable, as fields of the StateVar
    /// are private.
    pub fn new() -> Self {
        StateVar{
            v0: None,
            v1: None,
            v2: None,
            v3: None,
        }
    }

    /// Returns with the current value of the StateVar
    pub fn get(&self) -> Option<T> {
        self.v0
    }

    /// Update value stored in the state variable
    /// The dropped value, which is no longer stored
    /// is returned
    pub fn up(&mut self, value: T) -> Option<T> {
        let dropped = self.v3;
        self.v3 = self.v2;
        self.v2 = self.v1;
        self.v1 = self.v0;
        self.v0 = Some(value);
        dropped
    }

    /// Calculate the derivative
    pub fn deriv(&self, dt: f64) -> Option<<<T as Sub>::Output as Div<f64>>::Output> {
        match self {
            StateVar{
                v0: Some(y0),
                v1: Some(y1),
                .. } => Some((*y1-*y0)/dt),
            _ => None,
        }
    }
}

/// Optional functions for StateVar. These are only available
/// for some StateVar types. We require the generic type parameter
/// 'T' to have the Add<T> and Mul<f64> trait return with 'T'. This
/// makes sense, as numeric integrals use some king of weighted
/// average for integral calculations.
impl<T> StateVar<T> where
    T: Copy + Sub + Mul<f64, Output=T> + Add<Output=T>,
    <T as Sub>::Output: Div<f64> {
    /// Integrate state variable
    pub fn integ(&self, dt: f64) -> Option<T> {
        // Check how many previous value is available. Use
        // the apropriate integration method with available 
        // values.
        match self {
            StateVar{
                v0: Some(y0),
                v1: Some(y1), 
                v2: Some(y2),
                v3: Some(y3)
                } => {
                // Use Simpson's 3/8 rule. This is the best-case
                // scenario, and gives the least error.
                Some(((*y1+*y2)*3.0+*y0+*y3)*(3.0/8.0*dt))
            },
            StateVar{
                v0: Some(y0),
                v1: Some(y1),
                v2: Some(y2),
                ..
                } => {
                // Use Simpson's 1/3 rule
                Some((*y1*3.0+*y0+*y2)*(1.0/3.0*dt))
            },
            StateVar{
                v0: Some(y0),
                v1: Some(y1),
                ..
                } => {
                // Use Trapezoidal rule
                Some((*y0+*y1)*(0.5*dt))
            },
            StateVar{
                v0: Some(y0),
                ..
                } => {
                // Use Euler's rule
                Some(*y0*dt)
            },
            _ => None,
        }
    }
}