extern crate gtk;
extern crate gio;
extern crate cairo;
extern crate gdk;
extern crate glib;
extern crate ddagent;

use gtk::prelude::*;
use gio::prelude::*;
use std::f64::consts;

use std::thread;
use std::cell::RefCell;
use std::rc::Rc;
use std::sync::{
    Arc,
    RwLock,
    mpsc,
};

use ddagent::*;

fn new_populated_universe(agent_count: i32) -> Universe {
    let mut uni = Universe::new();
    for _ in 0..agent_count {
        uni.add_agent(Point::new(0,0));
    }
    uni
}

struct App {
    ui: UI,
    state: Arc<RwLock<State>>,
    image: Arc<RwLock<Option<Image>>>,
    world: Arc<RwLock<Option<Universe>>>,
    foreground_tx: mpsc::Sender<ForegroundEvent>,
}

impl App {
    pub fn new(app: &gtk::Application) -> Self {

        let ui = UI::new(app);

        let state = Arc::new(RwLock::new(State::new()));
        let world = Arc::new(RwLock::new(None));

        let (foreground_tx, foreground_rx) = mpsc::channel();

        let (draw_req, draw_req_rx) = mpsc::channel();
        
        let state_clone = Arc::clone(&state);
        let world_clone = Arc::clone(&world);
        let draw_req_clone = draw_req.clone();
        let tx_clone = foreground_tx.clone();
        thread::spawn(move || {
            while let Ok(event) = foreground_rx.recv() {
                match event {
                    ForegroundEvent::InitWorld(c) => {
                        *world_clone.write().unwrap() = Some(new_populated_universe(100));
                    }
                    ForegroundEvent::StepWorld(dt) => {
                        let mut world = world_clone.write().unwrap();
                        match &mut *world {
                            Some(wrld) => {
                                wrld.step(dt);
                                draw_req_clone.send(None).unwrap();
                            }
                            None => println!("You have to create a world first"),
                        }
                    }
                    ForegroundEvent::AutoStepChanged(state) => {
                        let mut s = state_clone.write().unwrap();
                        if state {
                            s.stepper_running = true;
                            let world_c = world_clone.clone();
                            let state_c = state_clone.clone();
                            let draw_req_c = draw_req_clone.clone();
                            let tx_c = tx_clone.clone();
                            glib::timeout_add(10, move || {
                                if !(state_c.read().unwrap().stepper_running) {
                                    return glib::Continue(false)
                                }
                                let mut w_mut = world_c.write().unwrap();
                                if let Some(wrld) = &mut *w_mut {
                                    wrld.step(0.1);
                                    tx_c.send(ForegroundEvent::StepWorld(0.1));
                                    draw_req_c.send(None).unwrap();
                                }
                                glib::Continue(true)
                            });
                        } else {
                            s.stepper_running = false;
                        }
                    }
                }
            }
        });

        let image = Arc::new(RwLock::new(None));
        let image_clone = image.clone();
        let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let world_clone = Arc::clone(&world);
        thread::spawn(move || {
            let mut size: (i32, i32, (f64, f64, f64, f64)) = (256, 256, (0.0, 0.0, 0.0, 1.0));
            while let Ok(s) = draw_req_rx.recv() {
                if let Some(v) = s {
                    size = v;
                }
                while let Ok(s) = draw_req_rx.try_recv() {
                    if let Some(v) = s {
                        size = v;
                    }
                }
                let color = size.2;
                let size = size.1.max(size.0);
                let mut img = Image::new(size, size);
                let wrld = world_clone.clone();
                img.with_surface(move |surface, _| {
                    let cr = cairo::Context::new(surface);
                    cr.set_source_rgba(color.0, color.1, color.2, color.3);
                    let sizef: f64 = size.into();
                    //cr.arc(sizef/2.0, sizef/2.0, sizef/3.0, 0.0, consts::PI*2.0);
                    //cr.fill();
                    let w: Vec<(u64, Object)>;
                    {
                        let w_ = wrld.read().unwrap();
                        match &*w_ {
                            Some(x) => w = x.objects.clone(),
                            None => w = Vec::new(),
                        }
                    }
                    for (_, obj) in w {
                        match obj {
                            Object::OAgent(x) => {
                                //println!("Agent");
                                let pos = x.pos.get().unwrap();
                                cr.arc(pos.x, pos.y, 5.0, 0.0, consts::PI*2.0);
                            }
                            Object::OItem(_) => {
                                println!("Item");
                            }
                            Object::OWall(_) => {
                                println!("Wall");
                            }
                        }
                    }
                    
                    cr.fill();

                    surface.flush();
                });
                //thread::sleep(std::time::Duration::from_secs(1));
                *image_clone.write().unwrap() = Some(img);
                tx.send("Redraw").unwrap();
            }
        });

        let draw_area = ui.content.clone();
        rx.attach(None, move |_| {
            draw_area.queue_draw();

            glib::Continue(true)
        });

        let image_clone = image.clone();
        let draw_req_clone = draw_req.clone();
        ui.content.connect_draw(move |w, cr| {

            if let Some(img) = &mut *image_clone.write().unwrap() {
                let width: f64 = w.get_allocated_width().into();
                let height: f64 = w.get_allocated_height().into();
                img.with_surface(move |surface, s| {
                    let size: f64 = (s.0).into();
                    let scale: f64 = width.max(height)/size;
                    //println!("size: {}, scale: {}", size, scale);
                    cr.scale(scale, scale);
                    /*if width > height {
                        cr.translate((width-size)/2.0, 0.0);
                    } else {
                        cr.translate(0.0, (height-size)/2.0);
                    }*/
                    cr.set_source_surface(&surface, 0.0, 0.0);
                    cr.paint();
                    cr.set_source_rgba(0.0, 0.0, 0.0, 0.0);
                });
            } else {
                draw_req_clone.send(Some((
                    w.get_allocated_width(),
                    w.get_allocated_height(),
                    get_widget_color(w),
                ))).unwrap();
            }
            
            gtk::Inhibit(true)
        });

        ui.content.connect_configure_event(move |w, e| {
            let (width, height) = e.get_size();

            if width > 0 || height > 0 {
                //println!("Requesting draw after resize");
                draw_req.send(Some((width as i32, height as i32, get_widget_color(w)))).unwrap();
            }

            true
        });

        ui.content.connect_style_updated(move |w| {
            // Send style changed event to draw thread
        });

        App { ui, state, foreground_tx, image, world }
    }

    pub fn show_all(&self) {
        self.connect_all();
        self.ui.window.show_all()
    }

    pub fn connect_all(&self) {
        let tx = self.foreground_tx.clone();
        self.ui.headerbar.buttons.init_world.connect_clicked(move |_| {
            tx.send(ForegroundEvent::InitWorld(100)).unwrap();
        });
        let tx = self.foreground_tx.clone();
        self.ui.headerbar.buttons.step.connect_clicked(move |_| {
            tx.send(ForegroundEvent::StepWorld(0.1)).unwrap();
        });
        let tx = self.foreground_tx.clone();
        self.ui.headerbar.buttons.autostep.connect_state_set(move |_, state| {
            tx.send(ForegroundEvent::AutoStepChanged(state)).unwrap();
            gtk::Inhibit(true)
        });
    }
}

fn get_widget_color(w: &gtk::DrawingArea) -> (f64, f64, f64, f64) {
    let context = w.get_style_context();
    let color = context.get_color(context.get_state());
    (color.red, color.green, color.blue, color.alpha)
}

struct Image(Option<Box<[u8]>>, (i32, i32));

impl Image {
    pub fn new(width: i32, height: i32) -> Self {
        Image(
            Some(vec![0; 4* width as usize * height as usize].into()),
            (width, height),
        )
    }

    fn with_surface<F: FnOnce(&cairo::ImageSurface, (i32, i32))>(&mut self, fnc: F) {
        struct Holder {
            data: Option<Box<[u8]>>,
            data_return: Rc<RefCell<Option<Box<[u8]>>>>
        }

        impl Drop for Holder {
            fn drop(&mut self) {
                *self.data_return.borrow_mut() =
                    Some(self.data.take().expect("Data is empty"));
            }
        }

        impl AsRef<[u8]> for Holder {
            fn as_ref(&self) -> &[u8] {
                self.data.as_ref().expect("Data is empty").as_ref()
            }
        }

        impl AsMut<[u8]> for Holder {
            fn as_mut(&mut self) -> &mut [u8] {
                self.data.as_mut().expect("Data is empty").as_mut()
            }
        }

        let img = self.0.take().expect("Image has no data");
        let return_loc = Rc::new(RefCell::new(None));
        {
            let holder = Holder {
                data: Some(img),
                data_return: return_loc.clone(),
            };

            let surface = cairo::ImageSurface::create_for_data(
                holder,
                cairo::Format::ARgb32,
                (self.1).0,
                (self.1).1,
                4* (self.1).0,
            ).expect("Can not create surface");

            fnc(&surface, self.1);
        }

        self.0 = Some(return_loc.borrow_mut().take().expect("No data returned"));
    }
}

struct State {
    pub value: f64,
    pub stepper_running: bool,
}

unsafe impl Send for State {}

impl State {
    pub fn new() -> Self {
        State { value: 0.0, stepper_running: false}
    }
}

enum ForegroundEvent {
    InitWorld(i32),
    StepWorld(f64),
    AutoStepChanged(bool),
}

struct UI {
    window: gtk::ApplicationWindow,
    headerbar: HeaderBar,
    content: gtk::DrawingArea,
}

impl UI {
    pub fn new(app: &gtk::Application) -> Self {
        let headerbar = HeaderBar::new();

        let window = gtk::ApplicationWindow::new(app);
        window.set_title("Rust cairo");
        window.set_titlebar(Some(&headerbar.header));

        let content = gtk::DrawingArea::new();
        window.add(&content);

        UI { window, headerbar, content }
    }
}

struct HeaderBar {
    header: gtk::HeaderBar,
    buttons: HeaderButtons,
}

impl HeaderBar {
    pub fn new() -> Self {
        let buttons = HeaderButtons::new();

        let header = gtk::HeaderBar::new();
        header.add(&buttons.init_world);
        header.add(&buttons.step);
        header.add(&buttons.autostep);
        header.set_show_close_button(true);

        HeaderBar { header, buttons }
    }
}

struct HeaderButtons {
    init_world: gtk::Button,
    step: gtk::Button,
    autostep: gtk::Switch,
}

impl HeaderButtons {
    pub fn new() -> Self {
        let init_world = gtk::Button::new_with_label("Init");
        let step = gtk::Button::new_with_label("Step");
        let autostep = gtk::Switch::new();

        HeaderButtons { init_world, step, autostep }
    }
}

fn main() {
    let application = gtk::Application::new(
        Some("com.ebalint.rust_cairo"),
        Default::default(),
    ).expect("Failed to initialize GKT application.");    

    application.connect_activate(|app| {
        App::new(app).show_all();
    });

    application.run(&[]);
}
