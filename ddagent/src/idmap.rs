use std::collections::{HashMap, BTreeSet};

/// A list of items which are accessable with unique ids.
///
/// When adding new items to the `IDMap`, it returns with the automatically
/// generated unique identifier. To retrieve the item, you should use that
/// id.
///
/// Removing the items can be done with the id key. After an item removed,
/// the corresponding id may be reused.
///
/// # Examples
///
/// Items can be retrieved by they unique id, which you can aquire when
/// inserting new items.
/// ```
/// # extern crate ddagent;
/// # use ddagent::IDMap;
/// let mut idmap = IDMap::new();
/// let apple_id = idmap.insert("Apple").unwrap();
/// let pear_id = idmap.insert("Pear").unwrap();
/// println!("{}", idmap.get(&apple_id).unwrap());
/// ```
///
/// The `IDMap` returns with `None` when a currently not use id is passed to
/// it.
/// ```
/// # extern crate ddagent;
/// # use ddagent::IDMap;
/// let mut idmap = IDMap::<String>::new();
/// let id: usize = 1;
/// assert_eq!(None, idmap.get(&id));
/// ```
///
/// It can store items of any kind. With a custom structure:
/// ```
/// # extern crate ddagent;
/// # use ddagent::IDMap;
/// struct Custom {
///     pub a: f64,
///     pub b: u32,
/// }
///
/// let mut idmap = IDMap::new();
/// let custom = Custom { a: 1.1, b: 3};
/// idmap.insert(custom);
/// ```
///
/// If you want to modify an item without removing and re-inserting,
/// you can get a mutable reference to it.
/// ```
/// # extern crate ddagent;
/// # use ddagent::IDMap;
/// struct Custom {
///     pub a: f64,
///     pub b: u32,
/// }
///
/// let mut idmap = IDMap::new();
/// let custom = Custom { a: 1.1, b: 3};
/// let id = idmap.insert(custom).unwrap();
///
/// let mutable = idmap.get_mut(&id).unwrap();
/// mutable.a = 2.2;
///
/// assert_eq!(2.2, idmap.get(&id).unwrap().a);
/// ```
pub struct IDMap<V> {
    // The base of the IDMap is a HashMap, where the items are stored with
    // their ids.
    map: HashMap<usize, V>,
    // Collecting the retrieved ids from removed item into a BTreeSet, as if
    // we cannot get more free ones, we should reuse.
    id_retrieved: BTreeSet<usize>,
    // The id_next field points to the next usable id value. If the retrieved
    // ids set is empty, we will use this for the newly added items.
    id_next: usize,
}

impl<V> IDMap<V> {
    /// Creates an empty `IDMap`
    pub fn new() -> Self {
        IDMap {
            map: HashMap::new(),
            id_retrieved: BTreeSet::new(),
            id_next: 0,
        }
    }

    /// Inserts a single element into the `IDMap`
    ///
    /// If there is no more room left in the `IDMap`, returns with
    /// `None`. Otherwise the return value will contain the id.
    pub fn insert(&mut self, item: V) -> Option<usize> {
        if self.id_next < std::usize::MAX {
            // Unused IDs are available, use them.
            self.map.insert(self.id_next, item);
            self.id_next += 1;
            Some(self.id_next -1)
        } else {
            // No unused ID is available, try to 
            // get some from the retrieved set.
            if self.id_retrieved.len() < 1 {
                // In this case the HashMap is at capacity,
                // the item could not be inserted
                None
            } else {
                // Use the minimum of the retrieved IDs
                let id = *self.id_retrieved.iter().next().unwrap();
                self.map.insert(id, item);
                self.id_retrieved.remove(&id);
                Some(id)
            }
        }
    }

    /// Removes an item from the `IDMap`
    ///
    /// If the requested type is not in the collection,
    /// returns `None`
    pub fn remove(&mut self, id: &usize) -> Option<V> {
        // Check if we can join the freed id with the next
        // id parameter
        if *id == self.id_next-1 {
            // It is possible to join the freed id
            self.id_next -= 1;
            // We also check if more ids from the retrieved
            // set can be joined
            while self.id_retrieved.remove(&self.id_next) {
                self.id_next -= 1
            }
        } else {
            // Joining is not possible - simply add the removed
            // id to the retrieved set
            self.id_retrieved.insert(*id);
        }
        self.map.remove(id)
    }

    /// Returns with an immutable reference to the value
    pub fn get(&self, id: &usize) -> Option<&V> {
        self.map.get(id)
    }

    /// Returns with a mutable reference to the value
    pub fn get_mut(&mut self, id: &usize) -> Option<&mut V> {
        self.map.get_mut(id)
    }

    /// Returns with both the key and the value
    pub fn get_key_value(&self, id: &usize) -> Option<(&usize, &V)> {
        self.map.get_key_value(id)
    }

    /// Returns with all item inside the IDMap
    pub fn get_all(&self) -> &HashMap<usize, V> {
        &self.map
    }

    /// Returns with a mutable reference to the stored items
    pub fn get_all_mut(&mut self) -> &mut HashMap<usize, V> {
        &mut self.map
    }

    /// Retrieves the number of items in the IDMap
    pub fn len(&self) -> usize {
        self.map.len()
    }

    /// Retrieves the keys currently present in the IDMap
    pub fn keys(&self) -> Vec<&usize> {
        self.map.keys().collect()
    }
}

impl<T> Default for IDMap<T> {
    fn default() -> Self {
        IDMap::new()
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn insert_get() {
        use super::*;

        let item = 10;
        let mut idmap = IDMap::new();
        let id = idmap.insert(item).unwrap();
        assert_eq!(idmap.get(&id).unwrap(), &item);
    }

    #[test]
    fn reusing_ids() {
        use super::*;

        let mut idmap = IDMap::new();
        let _ = idmap.insert(1).unwrap();
        let id = idmap.insert(2).unwrap();
        let _ = idmap.insert(3).unwrap();

        idmap.remove(&id);
        let id_reused = idmap.insert(4).unwrap();
        assert_ne!(id_reused, id);
    }

    #[test]
    fn get_mut() {
        use super::*;
        let mut idmap = IDMap::new();
        let id = idmap.insert(1).unwrap();
        let item = idmap.get_mut(&id).unwrap();
        *item = 20;
        assert_eq!(idmap.get(&id).unwrap(), &20);
    }
}