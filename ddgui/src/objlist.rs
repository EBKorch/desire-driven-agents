extern crate qmetaobject;
use qmetaobject::*;

// HashMap is used for QAbstractListModel role_names()
use std::collections::HashMap;

/// Describes a single item in the ObjectListModel. Contains
/// all required data for displaying.
/// 
/// The following fileds are defined:
/// 
///     - key: The key of the corresponding object in the
/// Universe. Used to get the properties of the simulated
/// object.
/// 
///     - position: Position of the object in meters. Required
/// to display the visual representation at proper location on
/// the GUI.
/// 
///     - variant: Type of the object, used to create different
/// visuals for different kind of objects.
pub struct ObjGuiDescriptor {
    pub key: usize,
    pub position: (f64, f64),
    pub variant: i32,
}

impl ObjGuiDescriptor {
    /// Creates a new description. The unique key, with which
    /// the original item can be retrieved should be passed
    /// along with the position and the variant.
    pub fn new(key: usize, x: f64, y: f64, variant: i32) -> Self {
        ObjGuiDescriptor {
            key,
            position: (x, y),
            variant,
        }
    }
}

/// Enumeration of the possible types of objects. This
/// corresponds to different visual representation.
pub enum ObjGuiVariant {
    Agent,
    Item,
    Wall,
}

/// Custom ListModel to display the simulated objects. Contains
/// a list of ObjGuiVariants.
#[derive(QObject, Default)]
pub struct ObjectListModel {
    base: qt_base_class!(trait QAbstractListModel),
    pub list: Vec<ObjGuiDescriptor>,
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    count_changed: qt_signal!(),
}

impl ObjectListModel {
    /// Adds a new ObjGuiDescriptor to the list.
    pub fn add(&mut self, key: &usize, position: (f64, f64), variant: ObjGuiVariant) {
        // Signal the start of appending a new item.
        let end = self.list.len();
        (self as &mut dyn QAbstractListModel).begin_insert_rows(end as i32, end as i32);
        // Create and push the new ObjGuiDescription
        self.list.push(
            ObjGuiDescriptor::new(*key, position.0, position.1, variant as i32)
        );
        // Signal the end of appending a new item. This informs the QML GUI
        // that the ListModel is changed.
        (self as &mut dyn QAbstractListModel).end_insert_rows();
        self.count_changed();
    }

    /// Removes the item with the specified keys from the list
    pub fn remove(&mut self, keys: &[usize]) {
        // Select the indices of the corresponding ids. These will
        // be the indices which will be removed from the list.
        let mut selected = self.list
            .iter()
            .enumerate()
            .filter_map(|(index, obj)| {
                if keys.contains(&obj.key) {Some(index)}
                else {None}
            }).collect::<Vec<usize>>();
        // Sort the vector to than iterate though it in reverse
        // order. This way we can avoid that we make indices
        // invalid by removing a lower indexed value and making
        // the indices shift.
        selected.sort_unstable();
        for &index in selected.iter().rev() {
            // Signal the start of item removal
            (self as &mut dyn QAbstractListModel).begin_remove_rows(index as i32, index as i32);
            self.list.remove(index);
            // Signal the end of item removal
            (self as &mut dyn QAbstractListModel).end_remove_rows();
        }
        // Emit the signal to notify the GUI about the size change
        self.count_changed();
    }

    // Informs the QML GUI that the data has changed
    pub fn update_range(&mut self, start: usize, end: usize) {
        // Start and end indices should be QModelIndex, so convert them
        let start = (self as &mut dyn QAbstractListModel).row_index(start as i32);
        let end = (self as &mut dyn QAbstractListModel).row_index(end as i32);
        // Emit the signal
        (self as &mut dyn QAbstractListModel).data_changed(start, end);
    }
}

impl QAbstractListModel for ObjectListModel {
    /// Returns with the number of items
    /// 
    /// As we only have a vector of items (a single column), the
    /// return value of the function is the length of the list
    fn row_count(&self) -> i32 {
        self.list.len() as i32
    }
    /// Returns with the corresponding field to the role
    /// 
    /// Roles are defined in role_names(), and should propagate
    /// all fields of ObjGuiDescriptor.
    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.list.len() {
            let item = &self.list[idx];
            if role == USER_ROLE {
                let mut ret = QVariantList::default();
                ret.push(item.position.0.into());
                ret.push(item.position.1.into());
                ret.into()
            } else if role == USER_ROLE + 1 {
                (item.key as i32).into()
            } else if role == USER_ROLE + 2 {
                item.variant.into()
            } else {
                QVariant::default()
            }
        } else {
            QVariant::default()
        }
    }
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE, "position".into());
        map.insert(USER_ROLE+1, "key".into());
        map.insert(USER_ROLE+2, "variant".into());
        map
    }
}