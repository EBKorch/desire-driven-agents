use crate::idmap::IDMap;
use geom2d::Point;

use super::{
    representation::{State, Shape},
    actions::{ ActionData, Action },
    effect::{ Effect, Change, DynamicChange },
};

use std::iter::FromIterator;
use std::collections::HashMap;

#[derive(Clone)]
pub enum Flag {
    StrLike(String),
    NumLike(f64),
}

pub struct FlagUnit {
    pub name: String,
    pub data: Flag,
}

impl FlagUnit {
    pub fn new(name: String, data: Flag) -> Self {
        FlagUnit { name, data }
    }
}

#[derive(Clone)]
pub struct Item {
    pub state: Option<State>,
    pub shape: Option<Shape>,
    pub flags: HashMap<String, Flag>,
    pub accepts: HashMap<String, ActionData>,
}

impl Item {
    pub fn new(state: Option<State>, shape: Option<Shape>, flagging: Vec<FlagUnit>, actions: Vec<Action>) -> Self {
        let mut accepts = HashMap::new();
        for action in actions {
            accepts.insert(action.name, action.data);
        }
        let mut flags = HashMap::new();
        for flag in flagging {
            flags.insert(flag.name, flag.data);
        }
        Item { state, shape, flags, accepts }
    }

    #[must_use = "Change only applied on return value, not self"]
    pub fn apply_try(&self, item: &Item) -> Self {
        let mut new_item = self.clone();
        new_item.apply(item);
        new_item
    }

    fn apply(&mut self, item: &Item) {
        if let Some(state) = &item.state {
            self.state = Some(state.clone());
        }
        if let Some(shape) = &item.shape {
            self.shape = Some(shape.clone());
        }
        for (name, flag) in &item.flags {
            match flag {
                Flag::StrLike(value) if value == &String::from("") => {
                    if let Some(_) = self.flags.get(name) {
                        self.flags.remove(name);
                    }
                }
                _ => {
                    self.flags.insert(name.clone(), flag.clone());
                }
            }
        }
        for (name, data) in &item.accepts {
            self.accepts.insert(name.clone(), data.clone());
        }
    }

    pub fn check_similarity(&self, item: &Item) -> f64 {
        // Similarity value
        let mut similar = 0.0;
        // Compare state
        if let Some(selfstate) = &self.state {
            if let Some(itemstate) = &item.state {
                similar -= (selfstate.position - itemstate.position).length();
            }
        }
        // Compare shape
        if let Some(selfshape) = &self.shape {
            if let Some(itemshape) = &item.shape {
                match itemshape {
                    Shape::Undefined => {
                        if let Shape::Undefined = selfshape {
                            similar += 1.0;
                        } else {
                            similar -= 1.0;
                        }
                    }
                    Shape::Area(segments) => {
                        let itempoints: Vec<Point> = segments.iter().map(
                            |x| x.p1
                        ).collect();
                        let selfpoints: Vec<Point> = segments.iter().map(
                            |x| x.p1
                        ).collect();
                        for (index, point) in itempoints.iter().enumerate() {
                            if index < selfpoints.len() {
                                similar -= (*point - selfpoints[index]).length();
                            }
                        }
                    }
                    Shape::Body(segments) => {
                        let itempoints: Vec<Point> = segments.iter().map(
                            |x| x.p1
                        ).collect();
                        let selfpoints: Vec<Point> = segments.iter().map(
                            |x| x.p1
                        ).collect();
                        for (index, point) in itempoints.iter().enumerate() {
                            if index < selfpoints.len() {
                                similar -= (*point - selfpoints[index]).length();
                            }
                        }
                    }
                }
            }
        }
        similar
    }

    pub fn check_base_similarity(&self, item: &Item) -> f64 {
        self.check_similarity(item)
    }

    // # Actions
    /// Get list of actions
    pub fn get_all_action(&self) -> &HashMap<String, ActionData> {
        &self.accepts
    }

    // # Effects
    /// Estimates the effect of the selected action
    pub fn get_effects(&self, action: &String, selfitem: &Item) -> Vec<Effect> {
        let mut effects = Vec::new();
        if let Some(action_data) = self.accepts.get(action) {
            for effect in &action_data.effects {
                if let Change::Dynamic(dynamic_change) = &effect.change {
                    /*match dynamic_change {
                        DynamicChange::Position(factor) => {
                            if self.state.is_none() { continue }
                            if selfitem.state.is_none() { continue }
                            let pos_target = self.state.as_ref().unwrap().position;
                            let pos_self = selfitem.state.as_ref().unwrap().position;
                            effects.push(
                                Effect::new(
                                    effect.hormon_name.clone(),
                                    Change::Absolute(factor*(pos_target-pos_self).length())
                                )
                            )
                        }
                    }*/
                    //pass
                } else {
                    effects.push(effect.clone());
                }
            }
        }
        effects
    }
}

impl Default for Item {
    fn default() -> Self {
        Item {
            state: None,
            shape: None,
            flags: HashMap::new(),
            accepts: HashMap::new(),
        }
    }
}

impl FromIterator<Item> for IDMap<Item> {
    fn from_iter<T: IntoIterator<Item = Item>>(iter: T) -> IDMap<Item> {
        let mut idmap = IDMap::new();
        for i in iter {
            idmap.insert(i);
        }
        idmap
    }
}


pub enum LinkedItem {
    Live((Item, Option<usize>)),
    Dead((Item, Option<usize>, Vec<usize>)),
    Ghost((Option<usize>, Vec<usize>)),
}

impl LinkedItem {
    pub fn as_item(linked_item: &LinkedItem) -> Item {
        match linked_item {
            LinkedItem::Live((item, _)) => item.clone(),
            LinkedItem::Dead((item, _, _)) => item.clone(),
            LinkedItem::Ghost((_, _)) => Item::default(),
        }
    }
}

pub struct Items {
    base: IDMap<Item>,
    all: IDMap<LinkedItem>,
    similar_thres: f64,
    similar_base_thres: f64,
}

impl Items {
    pub fn new(base_items: Vec<Item>) -> Self {
        let mut items = Items::default();
        items.base = base_items.into_iter().collect();
        items
    }

    pub fn set_similar_threshold(&mut self, thres: f64) -> f64 {
        if thres < 0.0 {
            self.similar_thres = 0.0;
        } else if thres > 1.0 {
            self.similar_thres = 1.0;
        } else {
            self.similar_thres = thres;
        }
        self.similar_thres
    }

    pub fn set_similar_base_threshold(&mut self, thres: f64) -> f64 {
        if thres < 0.0 {
            self.similar_base_thres = 0.0;
        } else if thres > 1.0 {
            self.similar_base_thres = 1.0;
        } else {
            self.similar_base_thres = thres;
        }
        self.similar_base_thres
    }

    pub fn find_most_similar(&mut self, item: Item) -> Option<usize> {
        let mut best: (usize, f64) = (0, 0.0);
        for (id, linked) in self.all.get_all() {
            if let LinkedItem::Dead(dead_linked) = linked {
                let value = item.check_similarity(&LinkedItem::as_item(linked));
                if value > best.1 {
                    best = (*id, value)
                }
            }
        }

        let id;
        if best.1 > self.similar_thres {
            id = self.all.insert(LinkedItem::Live((item, Some(best.0))));
            if let Some(LinkedItem::Dead(dead_linked)) = self.all.get_mut(&best.0) {
                dead_linked.1 = id;
            } else {
                unreachable!();
            }
        } else {
            id = self.all.insert(LinkedItem::Live((item, None)));
        }
        id 
    }

    pub fn insert_item(&mut self, item: Item, link: Option<&usize>) -> Option<usize> {
        if let Some(dead_id) = link {
            let live_id = match self.all.insert(LinkedItem::Live((item, None))) {
                Some(id) => id,
                None => return None,
            };
            let link_created = match self.all.get_mut(&dead_id) {
                Some(LinkedItem::Dead(dead_linked)) => {
                    dead_linked.1 = Some(live_id);
                    true
                }
                _ => false,
            };
            if link_created {
                match self.all.get_mut(&live_id) {
                    Some(LinkedItem::Live(live_linked)) => {
                        live_linked.1 = Some(*dead_id);
                    }
                    _ => unreachable!(),
                }
                Some(*dead_id)
            } else { None }
        } else {
            if let Some(ghost_id) = self.all.insert(LinkedItem::Ghost((None, Vec::new()))) {
                match self.all.insert(LinkedItem::Live((item, Some(ghost_id)))) {
                    Some(live_id) => {
                        match self.all.get_mut(&ghost_id).unwrap() {
                            LinkedItem::Ghost(ghost) => {
                                ghost.0 = Some(live_id);
                            }
                            _ => unreachable!(),
                        }
                        Some(ghost_id)
                    }
                    None => {
                        // We need to remove the ghost item
                        self.all.remove(&ghost_id);
                        None
                    }
                }
            } else {
                None
            }
        }
    }

    pub fn update_item(&mut self, item: Item, id: &usize) -> bool {
        let live_id = match self.all.get(id) {
            Some(LinkedItem::Dead((_, id, _))) => *id,
            Some(LinkedItem::Ghost((id, _))) => *id,
            _ => None,
        };
        if let Some(id) = &live_id {
            match self.all.get_mut(id) {
                Some(LinkedItem::Live((live_item, _))) => {
                    live_item.apply(&item);
                    true
                }
                _ => false,
            }
        } else { false }
    }

    pub fn release_item(&mut self, id: &usize) -> bool {
        let (live_id, ghost_features) = match self.all.get_mut(id) {
            Some(LinkedItem::Dead((_, id, _))) => {
                if id.is_none() { return false }
                let ret = id.unwrap().clone();
                *id = None;
                (ret, None)
            },
            Some(linked_item) => {
                if let LinkedItem::Ghost((Some(id), features)) = linked_item {
                    (id.clone(), Some(features.clone()))
                } else {
                    return false
                }
            },
            _ => return false,
        };
        match ghost_features {
            Some(features) => *self.all.get_mut(id).unwrap() = LinkedItem::Dead((
                Item::default(),
                None,
                features
            )),
            None => (),
        }
        let linked_item = self.all.remove(&live_id);
        match linked_item {
            Some(LinkedItem::Live((item, _))) => {
                match self.all.get_mut(id) {
                    Some(LinkedItem::Dead((dead_item, _, _))) => {
                        dead_item.apply(&item);
                        true
                    }
                    _ => false,
                }
            }
            _ => false,
        }
    }

    pub fn release_all(&mut self) {
        let live_ids: Vec<usize> = self.all.get_all().iter().filter_map(|(id, linked)| {
            match linked {
                LinkedItem::Dead((_, id, _)) => *id,
                LinkedItem::Ghost((id, _)) => *id,
                _ => None,
            }
        }).collect();
        for id in &live_ids {
            self.release_item(id);
        }
    }

    pub fn get(&self, id: &usize) -> Option<Item> {
        let (item , live_link, feature_link) = match self.all.get(id) {
            Some(LinkedItem::Dead((dead_item, live_link, feature_link))) => {
                (dead_item.clone(), live_link, feature_link)
            }
            Some(LinkedItem::Ghost((live_link, feature_link))) => {
                (Item::default(), live_link, feature_link)
            }
            _ => return None,
        };
        let mut item = item;
        if live_link.is_some() {
            if let Some(live_linked) = self.all.get(&live_link.unwrap()) {
                item.apply(&LinkedItem::as_item(live_linked));
            }
        }
        for feature_id in feature_link {
            if let Some(feature_item) = self.base.get(feature_id) {
                item.apply(feature_item);
            }
        }
        Some(item.clone())
    }

    pub fn get_all(&self) -> Vec<(&usize, Item)> {
        let mut items = Vec::new();
        for (id, linked_item) in self.all.get_all() {
            if let LinkedItem::Live(_) = linked_item {
                continue
            }
            if let Some(item) = self.get(id) {
                items.push((id, item));
            }
        }
        items
    }
}

impl Default for Items {
    fn default() -> Self {
        let base = IDMap::new();
        let all = IDMap::new();
        Items { base, all, similar_thres: 0.8, similar_base_thres: 0.95}     
    }
}

#[cfg(test)]
mod test {

    use super::{ Item, Action, ActionData, Effect, Change, DynamicChange, State, Shape };
    use crate::agent::hormon::{ HormonData, StaticChange };
    use geom2d::{ Point, Vector };

    #[test]
    fn item_get_effect() {
        let item = Item::new(
            Some(State::default()),
            Some(Shape::default()),
            Vec::new(),
            vec![
                Action::new(
                    String::from("Eat"),
                    ActionData::new(
                        Vec::new(),
                        vec![
                            Effect::new(
                                String::from("Hunger"),
                                Change::Absolute(-5.0),
                            ),
                            Effect::new(
                                String::from("Hunger"),
                                Change::Dynamic(DynamicChange::Position(1.0))
                            )
                        ],
                        Some(1.0),
                        true,
                        0,
                    )
                )
            ]
        );
        let mut hunger_hormon = HormonData::new(10.0, StaticChange::Disabled);
        let selfitem = Item::new(
            Some(State::new(Point::new(3, 4), Vector::new(0,0))),
            Some(Shape::default()),
            Vec::new(),
            Vec::new(),
        );
        let effects = item.get_effects(&String::from("Eat"), &selfitem);
        assert_eq!(1, effects.len());
        hunger_hormon.apply_change(&effects[0].change);
        assert_eq!(5.0, hunger_hormon.get_level());
        //hunger_hormon.apply_change(&effects[1].change);
        //assert_eq!(10.0, hunger_hormon.get_level());
    }
}