// External crates
extern crate qmetaobject;
use qmetaobject::*;

// Standard library
use std::cell::RefCell;

// Modules of this crate
use ddagent::*;
use crate::objlist::{
    ObjectListModel,
    ObjGuiVariant,
};
use crate::infolist::{
    AgentInformation,
};

/// World is a structure to bridge the QML GUI with
/// the underlaying logic. It uses the Universe structure
/// from ddagent crate as a member, and provides useful
/// functions to be used by the GUI. Can be defined in the
/// QML file.
#[allow(non_snake_case)]
#[derive(QObject, Default)]
pub struct World {
    // Required Qt part
    base: qt_base_class!(trait QObject),

    // The universe from ddagent crate. World is basically
    // a convenient QML object around it.
    uni: Universe,

    // List of removed items. This is necessary, as we only
    // only update the GUI every modelUpdateFrequency'th
    // step (see below).
    removed: Vec<usize>,

    // This property controls how often changes
    // caused by the simulation will be propagated
    // to the object list model (therefore to the GUI)
    modelUpdateFrequency: qt_property!(i32; NOTIFY model_update_frequency_changed),
    model_update_frequency_changed: qt_signal!(),
    counter: i32,

    // Function to step the world. This part also takes care of
    // propagating the changes to the object list model. This propagation
    // is only done every modelUpdateFrequency'th simulation step - 
    // this helps to improve performance of the GUI. 
    step: qt_method!(
        fn step(&mut self, dt: f64) {
            // The return value of uni.step() is the list of removed
            // items.
            self.removed.append(&mut self.uni.step(dt));

            // Check if we need to update the object list
            if self.counter >= self.modelUpdateFrequency {
                // With the remove() function of the object list model we
                // removed items from the list.
                self.objectList.borrow_mut().remove(&self.removed);
                self.removed.clear();
                // Boroow the objectList field as mutable, as we will use
                // it a lot in the following.
                let mut objectList = self.objectList.borrow_mut();
                // When updating the list model, iterate through all
                // of the items in the list, and retrieve the simulated
                // object with the key property.
                for item in &mut objectList.list {
                    // The universe return with Option<Object>, we need
                    // a match to extract the real object. The None arm
                    // theoretically cannot be reached as we already
                    // removed the nonexistent objects from the list.
                    match self.uni.get(&item.key) {
                        Some(obj) => {
                            match obj {
                                Object::OAgent(agent) => {
                                    let pos = agent.pos.get().unwrap();
                                    item.position = (pos.x, pos.y);
                                }
                                _ => continue,
                            }
                        }
                        None => continue,
                    }
                }
                // Emit the signal which informs the GUI about the update
                let length = objectList.list.len();
                objectList.update_range(0, length-1);
                // Reset the counter
                self.counter = 1;
            } else {
                // Increase counter
                self.counter += 1;
            }
        }
    ),

    // Custom ListModel for storing the objects and displaying
    // them. RefCell is required when used as a qt_property. A
    // signal is also defined to propagate changes to the GUI.
    objectList: qt_property!(RefCell<ObjectListModel>; NOTIFY object_list_changed),
    object_list_changed: qt_signal!(),

    // Method which adds an agent to the world. Returns false if
    // the object could not be added.
    addAgent: qt_method!(
        fn addAgent(&mut self, x: f64, y: f64) -> bool {
            // Create starting point
            let p = Point::new(x,y);
            // Add the agent to the world, which returns with
            // its unique key. If we get None, the function returns
            // false indicating that the object could not be added.
            let key;
            match self.uni.add_agent(p.clone()) {
                Some(id) => key = id,
                None => return false,
            }
            if let Object::OAgent(agent) = self.uni.get(&key).unwrap() {
                let pos = agent.pos.get().unwrap();
                self.objectList.borrow_mut().add(&key, (pos.x, pos.y), ObjGuiVariant::Agent);
            } else {
                // This is theoritically unreachable. If we somehow
                // end up here, it indicates a bug in the Universe.
                unreachable!(
                    "A different object is returned than what was added.
                    This is probably a bug in Universe."
                )
            }
            true
        }
    ),

    // Method which adds an item to the world. Returns false if
    // the object could not be added.
    addItem: qt_method!(
        fn addItem(&mut self, x: f64, y: f64) -> bool {
            // Create starting point
            let p = Point::new(x,y);
            // Add the item to the world, which returns with
            // its unique key. If we get None, the function returns
            // false indicating that the object could not be added.
            let key;
            match self.uni.add_item(p.clone(), vec![PropFlag::Food(10.0)]) {
                Some(id) => key = id,
                None => return false,
            }
            if let Object::OItem(item) = self.uni.get(&key).unwrap() {
                let pos = item.pos;
                self.objectList.borrow_mut().add(&key, (pos.x, pos.y), ObjGuiVariant::Item);
            } else {
                // This is theoritically unreachable. If we somehow
                // end up here, it indicates a bug in the Universe.
                unreachable!(
                    "A different object is returned than what was added.
                    This is probably a bug in Universe."
                )
            }
            true
        }
    ),

    // Method to retrieve number of items in world
    objCount: qt_method!(
        fn objCount(&self) -> u64 {
            self.uni.len() as u64
        }
    ),

    // The activeInfo property holds the "index" of the last updated
    // information object (currently only agentInfo - 1).
    activeInfo: qt_property!(i32; NOTIFY active_info_changed),
    active_info_changed: qt_signal!(),
    // The agentInfo property holds an AgentInformation property which
    // creates custom Qt properties, lists, etc, which is used by the GUI
    agentInfo: qt_property!(RefCell<AgentInformation>; NOTIFY agent_info_changed),
    agent_info_changed: qt_signal!(),

    /// Updates the agent information
    updateAgentInfo: qt_method!(
        fn updateAgentInfo(&mut self, key: usize) {
            // Check if the key is valid and points to an Agent structure
            if let Some(Object::OAgent(agent)) = self.uni.get(&key) {
                // Update the agentInfo
                self.agentInfo.borrow_mut().update(&key, agent);
                // Update the active info to point to the Agent
                if self.activeInfo != 1 {
                    self.activeInfo = 1;
                    self.active_info_changed();
                }
                self.agent_info_changed();
            }
        }
    )
}