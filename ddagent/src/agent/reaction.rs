use super::hormon::Hormon;

pub struct Reaction {
    hormon: Hormon,
    change: Change,
}

enum Change {
    Absolute(f64),
    Relative(f64)
}
