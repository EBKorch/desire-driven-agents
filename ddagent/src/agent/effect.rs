
#[derive(Clone)]
pub struct Effect {
    pub hormon_name: String,
    pub change: Change,
}

impl Effect {
    pub fn new(hormon_name: String, change: Change) -> Self {
        Effect { hormon_name, change }
    }
}

#[derive(Clone)]
pub enum Change {
    Absolute(f64),
    Relative(f64),
    Set(f64),
    Dynamic(DynamicChange),
}

#[derive(Clone)]
pub enum DynamicChange {
    Position(f64),
}
