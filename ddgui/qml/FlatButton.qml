import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Material.impl 2.12
import QtGraphicalEffects 1.12


// A reimplementation of the Button widget with custom styling
//
// Some extra features are added compared to the basic button, like
// icon, rounded corners, dynamic transparent background,
// selected property (aka sticky button), and customizable borders.
Item {
    id: root

    // Path to icon which will appear before the text
    property alias iconSource: ico.source
    // The text of the button
    property alias text: txt.text
    // Corner radius
    property alias radius: bgr.radius
    // Wheter or not display a border
    property bool borderless: false
    // Disable background makes the button transparent
    property bool nobackground: false
    // When highlighted, the style of the button is changed
    property bool highlighted: false
    // The button works sticky, one click activates, another deactivates it
    property bool selected: false

    onSelectedChanged: {
        root.state = selected ? "selected" : ""
    }

    signal clicked

    // Make the size of the Item match its background
    implicitWidth: bgr.width
    
    // Use a Rectangle as background, as it supports transparent background,
    // rounded corners, and elevation (Material shadows)
    Rectangle {
        id: bgr
        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
            right: txt.right
            rightMargin: -10
            topMargin: 5
            bottomMargin: 5
        }
        radius: 10
        // Use the default Material background. This is of course can be overriden
        color: Material.background
        // Make the background a bit smaller than the text, as when using
        // the Material theme, text is a bit too high compared to its content
        height: txt.height-10
        // Make the background a bit transparent
        opacity: 0.3
        // Add the elevation effect from Material theme
        layer.enabled: false
        layer.effect: ElevationEffect {
            elevation: bgr.Material.elevation
        }
        // Make the border width 0 when the button is created as borderless
        border.width: root.borderless ? 0 : 1
        // The border color is changed to a vivid color when the button is
        // set to highlighted
        border.color: parent.highlighted ? Material.accent : Material.foreground
        // By default, the elevation effect is not used, but this of course can
        // be changed
        Material.elevation: 0
    }

    // Image item to show an icon before the text. The Image widget is only
    // used to load the image, but it only serves as a mask to the
    // ColorOverlay, beacude we eant its color to be changeable.
    Image {
        id: ico
        anchors {
            top: parent.top
            left: parent.left
            bottom: parent.bottom
            margins: 8
            leftMargin: 0
        }
        // Ensure that the image wont be distorted
        fillMode: Image.PreserveAspectFit
        // When no source is set, change the width to zero avoiding shifting the
        // text
        width: source == "" ? 0 : parent.height
        source: ""
        // Make the icon rectengular, and match the height of the Image - which fills
        // the button in height
        sourceSize{width: width; height: height}
        // We do not show the image directly, just use as mask for the ColorOverlay
        visible: false
    }

    // ColorOverlay for colorful icons
    ColorOverlay {
        anchors.fill: ico
        source: ico
        color: txt.color
        // Antialiasing is must for pretty edges
        antialiasing: true
    }

    // Displayes the text in the button
    Text {
        id: txt
        anchors {
            top: parent.top
            left: ico.right
            bottom: parent.bottom
            leftMargin: ico.source == "" ? 11 : 0
        }
        verticalAlignment: Text.AlignVCenter
        color: Material.foreground
    }

    // This catched mouse events to provide the common button signals
    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }

    // The button has two states to imitate a sticky behaviour - "selected" and
    // the default (unselected) "" state. When the button is selected, the
    // background will have the accent color with highlighting the button.
    states: [
        State {
            name: "selected"
            PropertyChanges {target: txt; color: Material.background}
            PropertyChanges {
                target: bgr
                opacity: 1
                height: parent.height
                color: Material.accent
                radius: 0
                border.width: 0
            }
        }
    ]
    // Pretty transition for the background
    transitions: [
        Transition {
            from: "*"
            PropertyAnimation {target: txt; properties: "color"; duration: 50}
            PropertyAnimation {target: bgr; properties: "opacity, height, radius, color, border.width"; duration: 150}
        }
    ]
}
