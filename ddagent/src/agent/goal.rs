use super::{
    hormon::HormonData,
};

use std::collections::HashMap;


pub struct Goal {
    pub name: String,
    pub data: GoalData,
}

pub struct GoalData {
    pub connections: Vec<Connection>,
    pub importance: f64,
}

impl GoalData {
    pub fn new(connections: Vec<Connection>) -> Self {
        GoalData { connections, importance: 0.0 }
    }

    pub fn calc_importance(&mut self, bloodstream: &HashMap<String, HormonData>) {
        self.importance = self.get_calculated_importance(bloodstream);
    }

    pub fn get_calculated_importance(&self, bloodstream: &HashMap<String, HormonData>) -> f64 {
        let mut importance = 0.0;
        for connection in &self.connections {
            if let Some(hormon) = bloodstream.get(&connection.hormon) {
                importance += hormon.get_level()*connection.value;
            }
        }
        importance
    }
}

pub struct Connection {
    pub hormon: String,
    pub value: f64,
}
