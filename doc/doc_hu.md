% Desire driven agent - DDA
% Bognár Máté, Erdős Bálint
% 2020
\newpage
\tableofcontents
\newpage

# Intelligent Agent

Az inteligens ügynök alatt azokat az entitásokat értjük, amelyek önálló módon,
a környezetet szenzorokon keresztül megfigyelve előre meghatározok célokat
próbálnak elérni. Gyakran képesek a tapasztalat útján történő tanulásra.

Elterjedten használják őket etológiai kutatásokban, online asszitensek működtetésében,
valamint számítógépes játékok NPC irányítására.

# Desire-belief-intention

A DBI (desire-belief-intention) architektúra gyakran alkalmazott etológia kuttások
során. A DBI alkalmazása során az agent struktúra három részre osztható:

- **Belief**: Az agent prekoncpciói a világról és önmagáról. Tartalmazza azokat az információkat,
amelyek segítségével az agent döntéseket tud hozni

- **Desire**: Azoknak a céloknak (vágyaknak) a megfogalmazása, amelyek elérése / kielégítése az 
agent célja

- **Intention**: A döntési folyamat során kiválasztásra került cél. Miután ez rendelkezésre
áll, az agent elkezdi végrehajtani a tervet (**Plan**).

A terv végrehajtása során az agent eseményeket (**Event**) érhetik, amelyek megváloztathatják
az agent belső állapotát, úgy mint: új információk, új tervek, módosított célok.

Ezt az agent kialakítást etológia vizsgálatokon kívül ritkán használják. Ennek oka, hogy a
létező implementációk és környezetek elsősorban az etológia szempontjából támasztott
elvárásokat próbálja teljesíteni.

# Kitűzött célok

A megvalósított, saját architektúra kidolgozása során a DBI kialakítását vettük alapul.
A problémák, amelyeket amelyeket megoldani próbáltunk a feladat során, és a DBI ezekre nem 
biztosít módszereket.

## 1. Tanulás

Az agentnek minden setben szüksége van valamilyen tudásbázisra, amiben a világ és önmaga
reprezentációja kerül specifikálásra. Azonban minden ilyen ismeret manuális deklarálása
időigényes, valamint hibákat tartalmazhat, ezért nagy méretű, részletes, specifikus
tudásbázis kialakítása nem praktikus.

Erre megoldást kínál, hogyha az agent működése során a környezetből érkező ingerek alapján,
valamint a viselkedésének hatásait megfigyelve frissíti, bővíti a meglévő tudásbázist. Az
architektúránkat ezért olyan módon kell kialakítani, hogy lehetőséget biztosítson tanuló
algoritmusok beillesztésére.

## 2. Célok egyszerű specifikálása

Az esetek nagy többségében ismert, hogy az agentnek mik a feladatai, milyen célokat kell
elérnie. A felhasználást nagyban megkönnyíti, hogyha az agent létrehozásánál minimális
információt kell szolgáltatni a célok meghatározására. Ez együtt fog járni az architektúra
nagyobb komplexitásával, ugyanakkor lehetővé fogja tenni, hogy az agent újraspecifikálása
egyszerűen megtörténhessen.

E cél mögötti megfontolást az állapotgépek jelentik. Abban az esetben, hogyha az állapotgép
nagy számú állapottal rendelkezik, és bármelyik állapotból bármelyikbe eljuthat, szükséges
az összes lehetséges állapotátmenet definiálása. Ez nem csupán időigényes, de az
áttekinthetőséget is csökkenti.

Szeretnék tehát egy olyan megoldást létrehozni, ahol a célok, állapotok közötti váltásokat
az agent önmagától valósítja meg.

A teljese DDA architektúrának a célja lényegében: csökkenteni a specifikációs igényt, azaz
a DDA agent könnyen definiálható legyen minél több feladatra. Ez megnöveli a robosztusságát
is algoritmusnak, nehezebbé válik a "félrekonfigurálás".

## 3. Teljesítmény

Célunk volt, hogy a döntéshozást végrehajtó algoritmus valós időben működtethetű legyen. Ez 
lehetővé teszi, hogy a létrejött agent ne csupán szimulációban, hanem fizikailag létező
robotokban is felhasználható lesz.

# Az architektúra

Az architektúra bemutatása az alábbi módon került felbontásra: az Agent struktúra ismertetése,
majd az Agent integrálásának lehetőségei és interface-ek.

## Felhasznált eszközök

Az algoritmusok *Rust* programnyelven [1] kerültek implementálása. A programnyelv C jellegű,
teljesítménye hasonló, nem objektum orientált. A standard könyvtár magas szintű algoritmusokat
is tartalmaz: iterátorok, konténerek, stb. Erősen típusos nyelv.

A későbbi integrálás szempontjából hasznos, hogy Rust nyelven létrehozhatók Python modulok [3].

## Agent

A logikát az alábbi ábra szemlélteti:

![](images/Decision.png)

A működés alapját a célok (**Goals**) jelentik, valamint ezek kapcsolata a hormonokkal (**Hormons**). A
cél kiválasztása az alábbi módon történik: minden cél rendelkezik minden hormon szintjére vonatkoztatott
súllyal. A cél fontosságát a hormonszintek súlyozott összege jelenti. Minden iterációban kiválasztjuk
a legmagasabb fontossággal rendelkező célt, és arra törekszünk, hogy a fontosságát cskkentsük. Ez lényegében
egyet jelent a hormonok szintjének csökkentésével.

A **Hormon** struktúra legfontosabb tulajdonsága a pillanatnyi szintje. Ez az érték két féle képpen tud
megváltozni: külső hatások által (melyeket **Effect**-nek fogunk nevezni), valamint az idő függvényében
növekvő vagy csökkenő módon.

A cél kiválasztása után tehát olyan eseményeket (**Event**) szeretnénk végrehajtani, amelyek a hormonszinteket
csökkentik. Ez a tervezési szakasz az események sorozatát (**EventTree**) hozza létre. A kiindulási elemek az
összes olyan esemény, amely a kiválaszott célra ható hormonok szintjét úgy változtatja, hogy *összességében*
a cél fontossága csökken: azaz bizonyos hormonok szintje nőhet, amíg az eredőjük csökkenést eredményez.

Az esemény lényegében egy olyan összerendelés, amely az Agent által végrehajtható cselekvés (**Action**)
felparaméterezését jelenti: bizonyos cselekvéseknek lehet tárgya, vagy módosított jellemzője, például:

- mozgás: hova és milyen sebességgel

- töltés: melyik töltőállomás, mennyi ideig

Az **EventTree** létrehozása azon alapul, hogy a kivánt cselekvés végrehajtása csak akkor történhet meg,
hogyha bizonyos feltételek teljesülnek (**Reuirement**). A függőségekkel rendelkező események előtt ezért
végre kell hajtani azokat az eseményeket, amelyek a függőségeket kielégítik. Az **EventTree** ilyen módon
történő generálása egészen addig tart, amíg vagy az összes "levél" esemény függőség nélküli, vagy mire
elérjük a maximálisan meghatározott mélységet.

Az események ilyen módon kialakított láncolata a befejező eseménytől indul, és a kezdő eseménnyel végződik.
A kiértékeléshez tehát az utolsó elemtől az első felé haladunk: minden esemény hormonkra gyakorolt hatását
szimuláljuk egészen a végső eseményig. Az így létrejött, szimulált hormonszintek megmutatják, hogy
melyik eseménysor végrehajtása biztosítja a legnagyobb nyereséget (legnagyobb fontosság csökkenést). Ezek
után a eseményekben definiált cselekvéseket elkezdjük végrehajtani.

### Teljesítmény megfontolások

Az **EventTree** generálása költséges feladat lehet. Azonban ennek létrehozását nem kell minden iterációban
megtennünk, így a rendelkezésre álló erőforráshoz hangolható. Ritkábban kiértékelt EventTree késleltetés
okoz az Agent viselkedésében.

# Integrálás

Az Agent struktúra csupán a döntéshozást végzi, így valamilyen "testben" el kell helyeznünk (**Machine**).
A "test" feladata, hogy a környezetből érkező ingereket valamint tárgyakat (**Item**) az Agent számára értelmezhető formába képezze le, valamint hogy az Agent által kiadott cselekvéseket végrehajtsa. Ennek
a struktúrája az alábbi ábrán látható:

![](images/Flow.png)

## Item

A világban felfedezett objektumok absztrakt reprezántációja az alábbi információkat tartalmazza:

- alak

- pozíció és sebesség

- flag: név-érték párok, amelyek az objektum jellemzőit tartalmazzák (szín, érdesség, stb.)

- elérhető cselekvések listája

Ez utóbbi adat az egyik legfontosabb a működés szempontjából: definiálja azokat azokat a cselekvéseket,
amelyek a tárgyon végrehajthatóak, valamint hogy ennek milyen előfeltételei vannak, egyben megtalálható
benne a végrehajtáskor bekövetkező hatások. Ez utóbbi csak egy becslés, amellyel a Machine elősegítheti
a EventTree szimulációjának pontosítását.

A tárggyal rendelkező cselekvésekből kialakított események létrehozásakor az Agent minden olyan felfedezett
tárgyat figyelembe vesz, amelyen a cselekvés végrehajtható (lényegében behelyettesíti, mint paraméter). Ez
az a pont, ahol a tanulás megvalósítható az Agentben, hisz a cselekvés hatásának becslése pontosítható
a korábbi tapasztalatok alapján: amennyiben az Agent az Item flagjei alapján összehasonlítja a korábban
felhasznált elemekkel, akkor azok alapján is becsülhető a hatás (jelenleg ez nem implementált, a kialakítás
olyan, hogy "könnyen" megvalósítható).

### Item tárolás

Nem triviális az objektumok tárolása. Ez az alábbi példál keresztül kerül bemutatásra: amennyiben a Machine
felfedez egy új tárgyat, azt átadja az Agentnek, amely tárolásra kerül. Míg az objektum a Machine
"látóterében" van, nincs probléma. Azonban amint a Machine látóteréből kikerül, majd oda visszatér, a
Machinenek el kell döntenie, hogy az megegyezik-e egy korábban látott elemmel. Tévedés eseten vagy duplikált
elem kerül tárolásra az Agent belsejében, vagy nem a megfelelő elem kerül hozzárendelesre.

Ennek elkerülése érdekében a munkamemória-hosszútávú memória kialakításához hasonló reprezentáció került
implementálásra. A Machine által pillanatnyilag látott objektumok a rövidtávú memóriába kerülnek
(**LiveItems**), a korábban felfedezettek pedig a hosszútávúban kerülnek tárolásra (**DeadItems**). Amint a
Machine beazonosítja egy jelenleg látott elemet egy korábban felismerttel, akkor egy *link* kerül hozzáadásra
a **LiveItem**-hez, amely a megfelelő **DeadItem**-et hivatkozza. Így lehetőség van akár arra is, hogy a
amíg az objektum "szem előtt van" megváltoztassuk a hozzárendelést bármi fajta komolyabb számítás nélkül.

Mikor az objektumot a Machine már nem érzékeli, a *live* és *dead* variáns illesztésre kerül: a *live* adatok
felülírják a már meglévőket, valamint az úja adatok hozzáadódnak.

Objektum lekérése során az adatbázisből mindig a *dead* variánsokat keressük. Visszatérés előtt megvizsgáljuk,
hogy létezik-e kapcsolt **LiveItem**, és amennyiben igen, akkor egy **másolt** példányra illesztjük azt,
amivel vissztérünk. Minden egyes tárolt objektum egyedi azonosítóval rendelkezik, ez a hivatkozása alap mint
összekapcsolásnál, mind lekérésnél.

Ez a belső struktúra kívülről nem látható, a Machine-nek csupán az a feladata, hogy az objektumokat átadja,
informálja az Agent-öt az összerendelésről, valamint jelezze az objektum elvesztését. Lekérés során pedig
már az illesztett variánst kapjuk vissza.

## Effect

A Machine másik feladata tájékoztatni az Agent-öt a külső hatásokról. Ezek minden esetben 
hormonszint-változást idéznek elő. Ennek definiálására két lehetőség van:

- abszolút: a hormonszint kerül előírásra

- relatív: csökkenés vagy növekedés van előírva, amely a pillanatnyi szinthez adódik hozzá

Speciális **Effect** az úgynevezett dinamikus effekt: ez sem nem abszolút, sem nem relatív, hanem két hormon
vagy állapotváltozó közötti összefüggéssel definiált. Kizárólak az Agent tud ilyet generálni, és csak belső
felhasználásra szánt. Ilyen például a távolság hatását reprezentáló hormon: a hatás mértéke az Agent
pillanatnyi helyzetének és a célpont pozíciójának különbségétől függ.

\newpage

# Források

[1] Foundations of Distributed Artificial Intelligence: G. M. P. O'Hare, N. R. Jennings, 1996, ISBN: 978-0-471-00675-6

[2] Rust, https://www.rust-lang.org/, (Megtekintve: 2020.05.31.)

[3] PyO3 - Rust bindings for the Python interpreter, https://github.com/PyO3/pyo3, (Megtekintve: 2020.05.31.)