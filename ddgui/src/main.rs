extern crate qmetaobject;
use qmetaobject::*;
#[macro_use]
extern crate cstr;

mod objlist;
mod infolist;
mod world;

// Resources
// QML resource files
qrc!(qml_resource, "/" {
    "qml/main.qml" as "main.qml",
    "qml/SimBox.qml" as "SimBox.qml",
    "qml/WorldView.qml" as "WorldView.qml",
    "qml/TopPanel.qml" as "TopPanel.qml",
    "qml/FlatButton.qml" as "FlatButton.qml",
    "qml/SidePanel.qml" as "SidePanel.qml",
    "qml/Panel.qml" as "Panel.qml",
    "qml/AgentInfoComp.qml" as "AgentInfoComp.qml",
    "qml/qtquickcontrols2.conf" as "qtquickcontrols2.conf",
});
// Icon resources
qrc!(ico_resource, "/" {
    "qml/icons/chevron-up.svg" as "icon/chevron-up",
    "qml/icons/chevron-down.svg" as "icon/chevron-down",
    "qml/icons/chevron-right.svg" as "icon/chevron-right",
    "qml/icons/plus.svg" as "icon/plus",
    "qml/icons/cpu.svg" as "icon/cpu",
    "qml/icons/pizza.svg" as "icon/pizza",
    "qml/icons/droplet.svg" as "icon/droplet",
});

fn main() {
    // Register resources
    qml_resource();
    ico_resource();

    // Register the custom World type
    qml_register_type::<world::World>(cstr!("World"),1,0,cstr!("World"));

    // Create new QtQMlEngine
    let mut engine = QmlEngine::new();

    // Load the main qml to the engine
    engine.load_file("qrc:/main.qml".into());

    // Start the app
    engine.exec();
}
