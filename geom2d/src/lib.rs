mod primitives;

pub use primitives::{
    Point, Vector, Line, LineSegment,
    Transform, Distance, Intersection,
}; 
