extern crate qmetaobject;
use qmetaobject::*;

use ddagent::*;

use std::collections::HashMap;
use std::cell::RefCell;

/// Custom QObject to store Agent related informations
///
/// Contains two custom QAbstractList implementation as well:
///     - HormonListModel
///     - DesiresListModel
/// These two lists enumerates every desire and hormon the specific
/// agent has. By creating a custom list model for them, it is really
/// easy to display them on the GUI.
#[derive(QObject, Default)]
pub struct AgentInformation {
    base: qt_base_class!(trait QObject),
    // The Agent can be identified by its unique key
    key: qt_property!(usize; NOTIFY key_changed),
    key_changed: qt_signal!(),
    // Position stored as a list with length of 2
    position: qt_property!(QVariantList; NOTIFY position_changed),
    position_changed: qt_signal!(),
    // Velocity is stored just like position
    velocity: qt_property!(QVariantList; NOTIFY velocity_changed),
    velocity_changed: qt_signal!(),
    // Custom list model to enumerate hormons
    hormons: qt_property!(RefCell<HormonListModel>; NOTIFY hormons_changed),
    hormons_changed: qt_signal!(),
    // Custom list model for desires
    desires: qt_property!(RefCell<DesireListModel>; NOTIFY desires_changed),
    desires_changed: qt_signal!(),
}

impl AgentInformation {
    /// When called, the existing fielda are updated with the
    /// new information
    ///
    /// Also takes care to emmit the proper signals
    pub fn update(&mut self, key: &usize, agent: &Agent) {
        // Key ID
        self.key = *key;
        // Position
        let pos = agent.pos.get().unwrap();
        self.position = QVariantList::from_vec(vec![pos.x, pos.y]);
        // Velocity
        let vel = agent.vel.get().unwrap_or_else(|| Vector::new(0,0));
        self.velocity = QVariantList::from_vec(vec![vel.x, vel.y]);
        // Hormons
        self.hormons.borrow_mut().update_all(&agent.bloodstream);
        self.hormons_changed();
        // Desires
        self.desires.borrow_mut().update_all(&agent.desires);
        self.desires_changed();
    }
}

/// Make possible to create a QVariantList from Vector over elements
trait FromVec {
    fn from_vec<T: Into<QVariant>>(vec: Vec<T>) -> QVariantList;
}

impl FromVec for QVariantList {
    fn from_vec<T: Into<QVariant>>(vec: Vec<T>) -> QVariantList {
        let mut list = QVariantList::default();
        for elem in vec {
            list.push(elem.into());
        }
        list
    }
}

struct HormonElement {
    name: String,
    level: f64,
}

#[derive(QObject, Default)]
struct HormonListModel {
    base: qt_base_class!(trait QAbstractListModel),
    pub list: Vec<HormonElement>,
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    count_changed: qt_signal!(),
}

impl HormonListModel {
    fn add(&mut self, name: String, hormon: &Hormon) {
        // Signal the start of appending a new item.
        let end = self.list.len();
        (self as &mut dyn QAbstractListModel).begin_insert_rows(end as i32, end as i32);
        // Create and push the new ObjGuiDescription
        self.list.push(
            HormonElement { name, level: hormon.level }
        );
        // Signal the end of appending a new item. This informs the QML GUI
        // that the ListModel is changed.
        (self as &mut dyn QAbstractListModel).end_insert_rows();
        self.count_changed();
    }

    pub fn remove(&mut self, index: usize) {
        (self as &mut dyn QAbstractListModel).begin_remove_rows(index as i32, index as i32);
        self.list.remove(index);
        (self as &mut dyn QAbstractListModel).end_remove_rows();
    }

    pub fn update(&mut self, index: usize, hormon: &Hormon) {
        if self.list[index].level != hormon.level {
            self.list[index].level = hormon.level;
            let i = (self as &mut dyn QAbstractListModel).row_index(index as i32);
            (self as &mut dyn QAbstractListModel).data_changed(i, i);
        }
    }

    pub fn update_all(&mut self, hormons: &HashMap<String, Hormon>) {
        let mut processed = Vec::new();
        // Remove unneded and update the others
        let name_list = self.list.iter().map(|h| h.name.clone()).collect::<Vec<String>>();
        for (index, name) in name_list.iter().enumerate().rev() {
            match hormons.get(name) {
                Some(hormon) => self.update(index, hormon),
                None => self.remove(index),
            }
            processed.push(name)
        }
        // Add new ones
        for (name, hormon) in hormons.iter().filter(|(name, _)| !processed.contains(name)) {
            self.add(name.clone(), hormon);
        }
    }
}

impl QAbstractListModel for HormonListModel {
    /// Returns with the number of items
    /// 
    /// As we only have a vector of items (a single column), the
    /// return value of the function is the length of the list
    fn row_count(&self) -> i32 {
        self.list.len() as i32
    }
    /// Returns with the corresponding field to the role
    /// 
    /// Roles are defined in role_names(), and should propagate
    /// all fields of ObjGuiDescriptor.
    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.list.len() {
            let item = &self.list[idx];
            if role == USER_ROLE {
                QString::from(item.name.clone()).into()
            } else if role == USER_ROLE + 1 {
                item.level.into()
            } else {
                QVariant::default()
            }
        } else {
            QVariant::default()
        }
    }
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE, "name".into());
        map.insert(USER_ROLE+1, "level".into());
        map
    }
}

struct DesireElement {
    name: String,
    urge: f64,
}

#[derive(QObject, Default)]
struct DesireListModel {
    base: qt_base_class!(trait QAbstractListModel),
    pub list: Vec<DesireElement>,
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    count_changed: qt_signal!(),
}

impl DesireListModel {
    fn add(&mut self, desire: &Desire) {
        // Signal the start of appending a new item.
        let end = self.list.len();
        (self as &mut dyn QAbstractListModel).begin_insert_rows(end as i32, end as i32);
        // Create and push the new ObjGuiDescription
        self.list.push(
            DesireElement { name: desire.name.clone(), urge: desire.urge }
        );
        // Signal the end of appending a new item. This informs the QML GUI
        // that the ListModel is changed.
        (self as &mut dyn QAbstractListModel).end_insert_rows();
        self.count_changed();
    }

    pub fn remove(&mut self, index: usize) {
        (self as &mut dyn QAbstractListModel).begin_remove_rows(index as i32, index as i32);
        self.list.remove(index);
        (self as &mut dyn QAbstractListModel).end_remove_rows();
    }

    pub fn update(&mut self, index: usize, desire: &Desire) {
        if self.list[index].urge != desire.urge {
            self.list[index].urge = desire.urge;
            let i = (self as &mut dyn QAbstractListModel).row_index(index as i32);
            (self as &mut dyn QAbstractListModel).data_changed(i, i);
        }
    }

    pub fn update_all(&mut self, desires: &Vec<Desire>) {
        let mut processed = Vec::new();
        // Remove unneded and update the others
        let name_list = self.list.iter().map(|h| h.name.clone()).collect::<Vec<String>>();
        for (index, name) in name_list.iter().enumerate().rev() {
            let desire = desires.iter().filter(|desire| &desire.name == name).collect::<Vec<&Desire>>();
            if desire.len() > 0 {
                self.update(index, desire[0]);
            } else {
                self.remove(index)
            }
            processed.push(name)
        }
        // Add new ones
        for desire in desires.iter().filter(|desire| !processed.contains(&&desire.name)) {
            self.add(desire);
        }
    }
}

impl QAbstractListModel for DesireListModel {
    /// Returns with the number of items
    /// 
    /// As we only have a vector of items (a single column), the
    /// return value of the function is the length of the list
    fn row_count(&self) -> i32 {
        self.list.len() as i32
    }
    /// Returns with the corresponding field to the role
    /// 
    /// Roles are defined in role_names(), and should propagate
    /// all fields of ObjGuiDescriptor.
    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.list.len() {
            let item = &self.list[idx];
            if role == USER_ROLE {
                QString::from(item.name.clone()).into()
            } else if role == USER_ROLE + 1 {
                item.urge.into()
            } else {
                QVariant::default()
            }
        } else {
            QVariant::default()
        }
    }
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE, "name".into());
        map.insert(USER_ROLE+1, "urge".into());
        map
    }
}