use geom2d::{
    self,
    Distance,
    Intersection,
    Point,
    Vector,
    LineSegment,
};

use crate::statevariable::StateVar;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

#[derive(Clone)]
pub enum Object {
    OAgent(Agent),
    OWall(Wall),
    OItem(Item),
}

#[derive(Clone)]
pub struct Reaction {
    pub hormon_name: String,
    pub optimal_range: (f64, f64),
    pub characteristic: HFunc,
}

impl Reaction {
    pub fn new(hormon_name: &str, optimal_range: (f64, f64), characteristic: HFunc) -> Self {
        Reaction {
            hormon_name: hormon_name.into(),
            optimal_range: sort_range(optimal_range),
            characteristic,
        }
    }

    pub fn new_linear(hormon_name: &str, optimal_range: (f64, f64), weight: f64) -> Self {
        Reaction {
            hormon_name: hormon_name.into(),
            optimal_range: sort_range(optimal_range),
            characteristic: HFunc::Linear(weight),
        }
    }

    pub fn calc(&self, hormon_level: f64) -> f64 {
        let diff = ((self.optimal_range.0 + self.optimal_range.1)/2.0 - hormon_level).abs();
        if diff <= (self.optimal_range.0 - self.optimal_range.1)/2.0 {
            0.0
        } else {
            match self.characteristic {
                HFunc::Constant => 0.0,
                HFunc::Linear(m) => diff*m,
                HFunc::Quadratic(m) => diff.powi(2)*m,
                HFunc::Exponential(m) => f64::exp(diff)*m,
            }
        }
    }
}

fn sort_range(range: (f64, f64)) -> (f64, f64) {
    if range.0 > range.1 {
        (range.1, range.0)
    } else {
        range
    }
}

/// # Desire
/// 
/// Represents a need or will the agent has.
#[derive(Clone)]
pub struct Desire {
    pub name: String,
    pub reacts: Vec<Reaction>, // reactions to different hormons
    pub subdesires: Vec<Desire>,
    pub urge: f64,
    pub cost: Option<f64>,
    pub target: Option<(usize, Item)>,
}

impl Desire {
    pub fn new(name: &str, reacts: Vec<Reaction>) -> Self {
        Desire {
            name: name.into(),
            reacts,
            subdesires: Vec::new(),
            urge: 0.0,
            cost: None,
            target: None,
        }
    }

    pub fn calc_urge(&mut self, bloodstream: &HashMap<String, Hormon>) -> Vec<String> {
        let mut hormons = Vec::new();
        self.urge = self.reacts.iter().map(
            |r| match &bloodstream.get(&r.hormon_name) {
                Some(hormon) => {
                    hormons.push(r.hormon_name.clone());
                    r.calc(hormon.level)
                }
                None => 0.0,
        }).sum::<f64>();
        hormons
    }
}

#[derive(Clone)]
pub struct Hormon {
    pub level: f64,
    pub static_change: HFunc,
    pub reacts: Vec<PropFlag>,
}

impl Hormon {
    // Various new functions
    /// Creates a new hormon with the defined static_change function
    /// and with zero initial level
    pub fn new(static_change: HFunc, reacts: Vec<PropFlag>) -> Self {
        Hormon { level: 0.0, static_change, reacts }
    }
    /// Creates a new hormon with linear static_change and zero initial
    /// level
    pub fn new_lin(m: f64, reacts: Vec<PropFlag>) -> Self {
        Hormon { level: 0.0, static_change: HFunc::Linear(m), reacts }
    }
    pub fn new_quad(m: f64, reacts: Vec<PropFlag>) -> Self {
        Hormon { level: 0.0, static_change: HFunc::Quadratic(m), reacts }
    }
    pub fn new_exp(m: f64, reacts: Vec<PropFlag>) -> Self {
        Hormon { level: 0.0, static_change: HFunc::Exponential(m), reacts }
    }
    /// Creates a hormon with the specified initial level and the 
    /// provided static_change function
    pub fn new_init(level: f64, static_change: HFunc, reacts: Vec<PropFlag>) -> Self {
        Hormon { level, static_change, reacts }
    }

    /// The step function changes the hormon level according to the
    /// function defined in static_change, and the elapsed time
    pub fn step(&mut self, dt: f64) {
        let change = match &self.static_change {
            HFunc::Constant => return,
            HFunc::Linear(m) => m*dt,
            HFunc::Quadratic(m) => self.level*m*dt,
            HFunc::Exponential(m) => f64::exp(self.level)*m*dt,
        };
        self.level += change;
    }

    /// Function the apply the effect of a specific prop flag
    pub fn apply(&mut self, flag: &PropFlag) {
        for react in &self.reacts {
            match react {
                PropFlag::Food(factor) => {
                    if let PropFlag::Food(amount) = flag {
                        self.level -= factor*amount;
                        break
                    }
                }
                PropFlag::Water(factor) => {
                    if let PropFlag::Water(amount) = flag {
                        self.level -= factor*amount;
                        break
                    }
                }
            }
        }
        if self.level < 0.0 {
            self.level = 0.0;
        }
    }
}

#[derive(Clone)]
pub enum HFunc {
    Constant,
    Linear(f64),
    Quadratic(f64),
    Exponential(f64),
}

/// # Agent
/// 
/// Intelligent item
#[derive(Clone)]
pub struct Agent {
    pub pos: StateVar<Point>,
    pub vel: StateVar<Vector>,
    pub bloodstream: HashMap<String, Hormon>,
    pub backpack: Vec<(u64, Item)>,
    pub knowledge: HashMap<String, Vec<PropFlag>>,
    pub explored: HashMap<usize, Item>,
    pub map: Vec<Wall>,
    pub desires: Vec<Desire>,
}

#[derive(Clone)]
pub enum PropFlag {
    Food(f64),
    Water(f64),
}

fn is_same_group(f1: &PropFlag, f2: &PropFlag) -> bool {
    match f1 {
        PropFlag::Food(_) => if let PropFlag::Food(_) = f2 {true} else {false},
        PropFlag::Water(_) => if let PropFlag::Water(_) = f2 {true} else {false},
    }
}

fn has_flag(f1: &[PropFlag], f2: &PropFlag) -> bool {
    for flag in f1 {
        if is_same_group(&flag, &f2) {
            return true
        }
    }
    false
}

impl Agent {
    /// Creates an instance of this class with default
    /// position and velocity.
    pub fn new(posi: Point, velo: Vector) -> Self {
        // Create state variables and set their value
        let mut pos = StateVar::new();
        pos.up(posi);
        let mut vel = StateVar::new();
        vel.up(velo);
        // Default bloodstream
        let mut bloodstream = HashMap::new();
        bloodstream.insert(String::from("Eyeradius"), Hormon::new_init(10.0, HFunc::Constant, Vec::new()));
        bloodstream.insert(String::from("MaxSpeed"), Hormon::new_init(10.0, HFunc::Constant, Vec::new()));
        bloodstream.insert(String::from("CloseUrgeThres"), Hormon::new_init(1.0, HFunc::Constant, Vec::new()));
        bloodstream.insert(String::from("ReachDistance"), Hormon::new_init(0.5, HFunc::Constant, Vec::new()));
        bloodstream.insert(String::from("BaseUrge"), Hormon::new_init(1.0, HFunc::Constant, Vec::new()));
        // Default knowledge
        let mut knowledge = HashMap::new();
        knowledge.insert(String::from("Hunger"), vec![PropFlag::Food(-5.0)]);
        // Create the return Agent
        Agent {
            pos,
            vel,
            bloodstream,
            backpack: Vec::new(),
            knowledge,
            explored: HashMap::new(),
            map: Vec::new(),
            desires: Vec::new(),
        }
    }

    pub fn plan_path_to(&self, target: Point) -> Vector{
        (target - self.pos.get().unwrap())
            .resize(self.bloodstream.get("MaxSpeed").unwrap().level)
    }

    pub fn plan_explore_path(&self) -> Vector {
        Vector::new(8,6)
            .resize(self.bloodstream.get("MaxSpeed").unwrap().level)
    }

    pub fn step(&mut self, dt: f64, env: &HashMap<usize, Object>, to_remove: Arc<Mutex<Vec<usize>>>) {
        // Step the bloodstream hormons
        for (_, hormon) in self.bloodstream.iter_mut() {
            hormon.step(dt);
            //println!("{}: {}", name, hormon.level);
        }

        // Extract commonly used values
        let eyeradius = self.bloodstream.get("Eyeradius").unwrap().level;
        let selfpos = self.pos.get().unwrap();
        let urge_thres = self.bloodstream.get("CloseUrgeThres").unwrap().level;
        let reach_distance = self.bloodstream.get("ReachDistance").unwrap().level;

        // Use senses
        let mut found_items = see_around(self.pos.get().unwrap(), eyeradius, env);
        let pos = self.pos.get().unwrap();
        for expl in self.explored.iter() {
            if pos.dist(expl.1.pos) > eyeradius {
                found_items.insert(*expl.0, expl.1.clone());
            }
        }
        self.explored = found_items;

        // Early return if there is no desires :/
        if self.desires.is_empty() {
            return
        }

        // Calculate desire urges and cost
        let explored = &(self.explored);
        for desire in self.desires.iter_mut() {
            // Urge - done by the desire
            let hormons = desire.calc_urge(&self.bloodstream);
            // Cost - done by the agent
            for hormon in hormons {
                // Connection with the knowledge base! Check if the agent knows, 
                // how to decrease/increase the target hormon levels
                let knowledge = &(self.knowledge);
                if let Some(flaglist) = knowledge.get(&hormon) {
                    // Filter the items which are known to the agent
                    let mut items: Vec<(&usize, &Item)> = flaglist.iter().filter_map(
                        |f| locate_closest(&f, &selfpos, &explored)
                    ).collect();
                    // Search for the closest one - if any is found
                    if !items.is_empty() {
                        items.sort_by(
                            |i1, i2|
                            selfpos.dist(i2.1.pos).partial_cmp(&selfpos.dist(i1.1.pos)).unwrap()
                        );
                        desire.cost = Some(selfpos.dist(items[0].1.pos));
                        desire.target = Some((items[0].0.clone(), items[0].1.clone()));
                    } else {
                        desire.cost = None;
                        desire.target = None;
                    }
                }
            }
        }
        // Sort desires - if the urge difference is small, the one with lower
        // cost will be choosen. a and b is swapped to make the first element
        // of the vector the choosen one
        self.desires.sort_by(
            |a, b| if (a.urge-b.urge).abs() < urge_thres {
                if let (Some(acost), Some(bcost)) = (a.cost, b.cost) {
                    bcost.partial_cmp(&acost).unwrap()
                } else {
                    b.urge.partial_cmp(&a.urge).unwrap()
                }
            } else {
                b.urge.partial_cmp(&a.urge).unwrap()
            }
        );

        // Estimate the state in the future when the desire is met. 
        // ToDo

        // Check if we can reach the target. If not, plan the path and calculate
        // the currently required velocity
        //println!("Selected: {}", &self.desires[0].name);
        let vel = match &self.desires[0].target {
            Some((id, item)) => {
                let mut to_rem = to_remove.lock().unwrap();
                if item.pos.dist(selfpos) < reach_distance &&
                    to_rem.iter().filter(|&x| x == id).count() == 0 {
                    // Use the item!
                    to_rem.push(id.clone());
                    drop(to_rem);
                    for flag in &item.props {
                        for (_, hormon) in self.bloodstream.iter_mut() {
                            hormon.apply(&flag);
                        }
                    }
                    // Rest a bit
                    Vector::new(0,0)
                } else {
                    self.plan_path_to(item.pos)
                }
            }
            None => self.plan_explore_path()
        };

        self.vel.up(vel);
    }
}

fn locate_closest<'a>(flag: &PropFlag, selfpos: &Point, explored: &'a HashMap<usize, Item>) -> Option<(&'a usize, &'a Item)> {
    // Search environment
    let mut ranked_filtered = explored.iter().filter(
        |&i| has_flag(&i.1.props, &flag)
    ).collect::<Vec<(&usize, &Item)>>();

    // Rank by distance from current location
    ranked_filtered.sort_by(
        |a, b|
        selfpos.dist(a.1.pos).partial_cmp(
            &selfpos.dist(b.1.pos)
        ).unwrap()
    );

    // Return with its position if anything is found
    if !ranked_filtered.is_empty() {
        Some(ranked_filtered[0])
    } else {
        None
    }
}

impl Physics for Agent {
    fn pos(&self) -> Point {
        self.pos.get().unwrap()
    }
    fn vel(&self, dt: f64) -> Vector {
        self.vel.integ(dt).unwrap_or_else(|| Vector::new(0,0))
    }
}

/// # Sense
/// 
/// Eyes
fn see_around(from: Point, radius: f64, env: &HashMap<usize, Object>) -> HashMap<usize, Item>{
    let mut saw_items = HashMap::new();
    for (id, item) in env.iter() {
        if let Object::OItem(item) = item {
            if item.pos.dist(from) < radius {
                //println!("Found at: {}", item.pos);
                saw_items.insert(*id, item.clone());
            }
        }
    }
    saw_items
}


/// # Wall
/// 
/// A simple collider which does not change in time
#[derive(Clone)]
pub struct Wall {
    pub pos: Point,
    pub points: Vec<Point>,
}

impl Wall {
    fn new<T, U, V, W>(x: T, y: U, points: Vec<(V, W)>) -> Self where
        T: Into<f64> + Copy,
        U: Into<f64> + Copy,
        V: Into<f64> + Copy,
        W: Into<f64> + Copy {
        // Convert the list of coordinates to list of points and
        // create the instance
        Wall {
            pos: Point::new(x, y),
            points: points.iter().map(|t| Point::new(t.0, t.1)).collect(),
        }
    }

    fn segments(&self) -> Option<Vec<LineSegment>>{
        if self.points.len() < 2 {
            return None
        }
        Some(
            self.points.iter().zip(self.points.iter().skip(1))
                .map(|(p1, p2)| LineSegment::new(p1.x, p1.y, p2.x, p2.y)).collect()
        )
    }
}

/// # Item
/// 
/// Structure to represent non-thinking objects which also do not have
/// any physics simulation.
#[derive(Clone)]
pub struct Item {
    pub pos: Point,
    pub props: Vec<PropFlag>,
}

impl Item {
    /// Create a default item
    pub fn new(pos: Point, props: Vec<PropFlag>) -> Self {
        Item {
            pos,
            props,
        }
    }

    /// Create a new item with no flags or requirements
    pub fn new_basic(pos: Point) -> Self {
        Item {
            pos,
            props: Vec::new(),
        }
    }
}


/// # Physics
///
/// Only provides the move and slide function and the Physics trait

// Objects which provides physics functionality should implement this trait
pub trait Physics {
    fn pos(&self) -> Point;
    fn vel(&self, dt: f64) -> Vector;
}

// Moves the object and detects collision. When colliding, the object will
// slide along the edge.
pub fn move_and_slide<T: Physics>(item: &T, env: &HashMap<usize, Object>, dt: f64) -> Point {
    // Calculate the interpolated trajectory for 'dt' time frame. We
    // will use this vector to check for collision, and remove 
    // impossible components of the rajectory
    let pos = item.pos();
    let mut vel = item.vel(dt)*dt;

    let mut disallowed: Vec<Vector> = vec![vel];

    let ahead: f64 = 0.01;
    let mut traj = LineSegment::new_from_vec(pos, vel.resize(vel.length()+ahead));
    // Check collision with every object in the environment: needs
    // optimization
    for (_, obj) in env {
        match obj {
            Object::OAgent(_) => {
                // No collision between agents
            },
            Object::OWall(wall) => {
                // Walls are possive, and generate collision. Get the
                // collision shape and check collision with every line
                // segment inside it.
                let segments = match wall.segments() {
                    Some(shape) => shape,
                    None => continue,
                };
                for segm in &segments {
                    let seg = segm.add_length(0.05);
                    if seg.intersect(traj).is_some() {
                        // Retrieve the normal vector of the collided segment
                        let perp = seg.dir().perpend();
                        if perp.angle_to(vel).abs()*2.0 > std::f64::consts::PI {
                            disallowed.push(perp);
                        } else {
                            disallowed.push(-perp);
                        }
                        // Remove the colliding component from velocity
                        vel = vel - vel.component(&perp);
                        // Check if velocity contradicts with disallowed array. If 
                        // it does, early exit with no position change
                        for d in &disallowed {
                            if d.angle_to(vel).abs()*2.0 > std::f64::consts::PI {
                                return pos
                            }
                        }
                        // Update trajectory
                        traj = LineSegment::new_from_vec(pos, vel.resize(vel.length() + ahead));
                    }
                }
            }
            Object::OItem(_) => {
                // No collision with items
            }
        }
    }

    // Return with the end of the trajectory
    pos + vel
}