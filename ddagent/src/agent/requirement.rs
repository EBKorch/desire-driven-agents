use crate::agent::{
    item::Item,
};

#[derive(Clone)]
pub struct Event {
    pub action_name: String,
    pub target: Option<Target>,
    pub profit: Option<f64>,
}

impl Event {
    pub fn new(action_name: String, target: Option<Target>) -> Self {
        Event{ action_name, target, profit: None }
    }

    pub fn set_profit(&mut self, profit: f64) {
        self.profit = Some(profit);
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Target {
    Item(usize),
    Inventory(),
}

#[derive(Clone)]
pub enum Requirement {
    Action(Event),
    HormonMinimumLevel(String, f64),
    HormonMaximumLevel(String, f64),
    Item((Item, String)),
}
