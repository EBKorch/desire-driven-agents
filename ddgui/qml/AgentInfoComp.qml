
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12

// Custom QML type which on its own display information about an Agent
//
// It gains every information from the AgentInformation Qt type
// (implemented in Rust at infolist.rs), so an instance of this widget
// requires its agentInfo property to set a valid AgentInformation
// instance (it is usually provided by the World custom type).

Item {
    id: agent_root
    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
    }
    // Reference to the AgentInformation object
    property var agentInfo
    // Height is calculated from the childrens individual height
    height: 2*hormonstxt.implicitHeight + props.implicitHeight + hormonlist.implicitHeight + desirelist.implicitHeight + 50
    // Grid layout to hold key-value pairs (like position, etc)
    GridLayout {
        id: props
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        // The gris has two columns: first column with the property name, second
        // one for the value.
        columns: 2

        // Title: Agent
        Text {
            font { pointSize: 16; bold: true; capitalization: Font.AllUppercase }
            text: "Agent"
            Layout.columnSpan: 2
        }

        // Position header
        Text {
            text: "Position"
            font { pointSize: 12; bold: true }
            Layout.columnSpan: 2
        }
        // The two coordinates
        Text { text: "X" }
        Text { text: agentInfo ? agentInfo.position[0] : ""}
        Text { text: "Y" }
        Text { text: agentInfo ? agentInfo.position[1] : "" }
        // Spacer
        Item { Layout.rowSpan: 2; height: 5 }

        // Velocity header
        Text {
            text: "Velocity"
            font {pointSize: 12; bold: true}
            Layout.columnSpan: 2
        }
        // The two components of the velocity
        Text { text: "X" }
        Text { text: agentInfo ? agentInfo.velocity[0] : "" }
        Text { text: "Y" }
        Text { text: agentInfo ? agentInfo.velocity[1] : "" }
        // Spacer
        Item { Layout.rowSpan: 2; height: 5 }
    }

    // Hormons list
    // Title
    Text {
        id: hormonstxt
        anchors{top: props.bottom; left: parent.left; topMargin: 5}
        text: "Hormons"
        font {pointSize: 12; bold: true}
    }
    // The hormons are displayed in a single column, dynamically generated
    // by a repeater, while the model is provided (and updated) by the
    // AgentInformation object
    ColumnLayout {
        id: hormonlist
        anchors {
            top: hormonstxt.bottom
            left: parent.left
            right: parent.right
            topMargin: 15
        }
        // Create the hormons from the model: create card for every
        // hormon
        Repeater {
            // Make sure that only refrence the hormons field if the agenInfo
            // is actually set, otherwise we will get errors.
            model: agentInfo == undefined ? undefined : agentInfo.hormons
            delegate: Pane {
                Layout.fillWidth: true
                Material.elevation: 1
                ColumnLayout {
                    Text { text: name; font {italic: true} }
                    Text { text: level }
                }
            }
        }
    }

    // Desires list
    // Title
    Text {
        id: desirestxt
        anchors{top: hormonlist.bottom; left: parent.left; topMargin: 15}
        text: "Desires"
        font {pointSize: 12; bold: true}
    }
    // Desires are displayed in a single column. The list is provided
    // by the AgentInformation object
    ColumnLayout {
        id: desirelist
        anchors {
            top: desirestxt.bottom
            left: parent.left
            right: parent.right
            topMargin: 15
        }
        // Create card for every desire
        Repeater {
            // Make sure that only refrence the desires field if the agenInfo
            // is actually set, otherwise we will get errors.
            model: agentInfo == undefined ? undefined : agentInfo.desires
            delegate: Pane {
                Layout.fillWidth: true
                Material.elevation: 1
                ColumnLayout {
                    Text { text: name; font {italic: true} }
                    Text { text: urge }
                }
            }
        }
    }
}