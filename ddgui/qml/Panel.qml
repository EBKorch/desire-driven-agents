import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12

// Simple Panel which can accomodate more items than its size
//
// When the Panel becomes full, it reveales a scrollbar to view
// the full list of items. It has an opaque background.
Item {
    id: root

    // The Panel has a title which is above its content
    property string title
    // The height of the title is not changeable, but for some
    // application we need to know its height
    readonly property alias titleHeight: titlepane.height
    // To populate the panel, set the data to your desires item. Only
    // set one root item, then make every other its child.
    default property alias data: contentinner.data

    // Provides the background
    Pane {anchors.fill: parent}

    // Scrollable content
    Flickable {
        anchors {
            top: titlepane.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentWidth: parent.width
        contentHeight: contentinner.height + 50

        // Populates the Flickable with items.
        Pane {
            id: contentinner
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                leftMargin: 30
                rightMargin: 30
            }
            height: children[0].implicitHeight
        }
    }

    // The title, which has its own background to mask the content. This
    // is the last item to make sure that it always rendered above everything
    // else.
    Pane {
        id: titlepane
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        Text {
            id: titletext
            anchors.fill: parent
            text: title
            font.pointSize: 18
            color: Material.foreground
        }
    }
}