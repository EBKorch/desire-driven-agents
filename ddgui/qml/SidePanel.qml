import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.12

// A panel which is intended to be used at the left side of a window
//
// It provides multiple pages: the "Inspector" and the "Add object"
// page. It can be hidden as well.
Item {
    id: root

    /// Signals
    signal hideClicked
    signal addObjectClicked

    // These properties provides the information to the Inspector tab to
    // be able to display the Agent properties
    property var activeInfo
    property var agentInfo
    property var activeKey

    // Update the Inspector when the activeInfo changes. This ensures that
    // the informations in are always up to date.
    onActiveInfoChanged: {
        // The activeInfo indicates which slot to use (currently only
        // agentInfo but itemInfo is expected). When the correct branch
        // is selected, load the proper component, and bind the info
        // property to it. Binding is required, as a simple value passing
        // will result in data copy, and not a reference (and for continues
        // update we need the reference).
        // We also store the key of the newly selected object to be able to
        // reference it (the key is unique identifier for objects in the World.
        switch (activeInfo) {
            case 1:
                infoloader.sourceComponent = agent_comp;
                infoloader.item.agentInfo = Qt.binding(function() {return agentInfo});
                root.activeKey = agentInfo.key;
                break;
        }
    }

    // Target object is the item which will be added when the user
    // clickes on the add button.
    property var targetObject: "Agent"
    // Index of the current page. Created an alias to it to be able to
    // retrieve iit in an outside module.
    property alias currentPage: swipeview.currentIndex

    // Set a background to the side panel, and elevate it (this will cast
    // shadows on the edges)
    Pane {
        anchors.fill: parent
        Material.elevation: 1
    }

    // The content of the side panel is inside a SwipeView. Every page uses
    // the Panel custom type.
    SwipeView {
        id: swipeview
        anchors.fill: parent
        clip: true
        currentIndex: 1
        // The inspector dynamically loads the proper information displaying
        // component. It is contolled in the onActiveInfoChanged() signal.
        Panel {
            id: inspector_panel
            title: "Inspector"
            Loader {
                id: infoloader
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
            }
        }
        // The add object plane provides UI to object adding
        Panel {
            id: addobject_panel
            title: "Add object"
            GridLayout {
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                columns: 2
                height: 200

                // The type can be selected with a combo box
                Label {
                    text: "Type"
                    Layout.fillWidth: true
                }
                ComboBox {
                    id: targettype
                    model: ["Agent", "Food", "Water", "Wall"]
                    Layout.alignment: Qt.AlignRight
                    onCurrentTextChanged: {
                        targetObject = currentText
                    }
                }

                // This is a simple spacer
                Item {
                    Layout.columnSpan: 2
                    height: 20
                }

                // The add button is a round button with a plus sign. It
                // rotates 45 degrees when clicked indicating that object
                // adding is active. When clicked agen it rotates back to a
                // plus sign.
                RoundButton {
                    id: addobjectbutton
                    text: "+"
                    Layout.columnSpan: 2
                    Layout.alignment: Qt.AlignHCenter
                    // Make it rectengular and set the radoius in such a way
                    // that we get a circle.
                    radius: height/2
                    width: height
                    highlighted: true
                    // Elevate it to cast shadows
                    Material.elevation: 1
                    // When clicked, propagate the event to the root item, and
                    // rotate the button.
                    onClicked: {
                        root.addObjectClicked();
                        state = state == "" ? "rotated" : "";
                    }
                    states: [
                        State {
                            name: "rotated"
                            PropertyChanges {target: addobjectbutton; rotation: -45}
                        }
                    ]
                    transitions: [
                        Transition {
                            from: "*"
                            PropertyAnimation{target: addobjectbutton; properties: "rotation"}
                        }
                    ]
                }
            }
        }
    }

    // An extra button provides the ability to hide the SidePanel. When
    // clicked, the panel collapses, and only this button will be shown.
    Item {
        id: hideitem
        anchors {
            top: parent.top
            right: parent.right
            rightMargin: 10
        }
        // Position the image in such a way, that it is line with the
        // title, and is about the same size.
        height: addobject_panel.titleHeight
        width: 20
        // The icon is a simple arrow, with a ColorOverlay to be able
        // to change it to white, when dark theme is selected
        Image {
            id: hideicon
            anchors.fill: parent
            source: "qrc:///icon/chevron-up"
            rotation: -90
            fillMode: Image.PreserveAspectFit
            sourceSize.width: width
            sourceSize.height: height
            visible: false
        }
        ColorOverlay {
            anchors.fill: hideicon
            source: hideicon
            color: Material.foreground
            antialiasing: true
        }
    }

    // Make the close event be catched on the title and on the hideitem
    MouseArea {
        anchors {
            top: parent.top
            left: parent.left
            bottom: hideitem.bottom
            right: root.right
        }
        // When clicked, hide the side panel, but also propaget this event
        // to the root item to be able to catch in in an outside module.
        onClicked: {
            root.hideClicked();
            root.state = (root.state == "closed" ? "" : "closed");
        }
    }

    // Looks like it is not used, but not do not change in this branch, just
    // note it.
    ListModel {
        id: inspectormodel
    }

    // The closing effect is achived with anchor changing. The left side is freed,
    // while the right side is set to the previously used target for the left side.
    // Combined with a transition, this creates an effect of a moving panel to
    // the left.
    states: [
        State {
            name: "closed"
            AnchorChanges {target: root; anchors{right: parent.left; left: undefined}}
            PropertyChanges {target: root; anchors.rightMargin: 2}
        }
    ]

    transitions: [
        Transition {
            to: "*"
            AnchorAnimation {}
        }
    ]

    /// Information components
    // These components are specialized to display relevant information
    // about different kinf of objects.

    // Agent information
    Component {
        id: agent_comp
        AgentInfoComp {}
    }
}