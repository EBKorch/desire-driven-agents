import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Item {
    id: root

    /// Properties which should be set externally
    // World object
    property var world
    // List of items in th world

    /// Properties which can be changed
    // Initial scale of the canvas.
    property int metersInWidth: 50
    property var _scale: width/metersInWidth
    // How dense the grid is. Value indicates the physical distance
    // between the gridlines
    property int gridDensity: 5

    /// Color settings
    // Color of the grid
    property var gridColor: Material.foreground//Material.Grey
    // Color of the walls
    property var wallColor: Material.foreground//Material.Black

    /// Signals
    signal clicked(real xPos, real yPos)
    signal updateFinished()
    signal inspected(var WID)

    /// Functions
    function requestPaint() {
        canvas.requestPaint()
    }

    // Canvas, where the grid and static items are drawn
    Canvas {
        id: canvas
        anchors.fill: parent

        //renderStrategy: Canvas.Threaded
        renderTarget: Canvas.FramebufferObject

        onPaint: {
            /// Reset canvas
            var ctx = getContext("2d");
            ctx.reset();

            /// Draw grid
            ctx.beginPath();
            var step = root.gridDensity*root._scale;
            var l = 0;
            // Draw X lines
            while(l <= width) {
                // Along X axis
                ctx.moveTo(l,0);
                ctx.lineTo(l,height);
                // Aling Y axis
                ctx.moveTo(0,l);
                ctx.lineTo(width,l);
                // Increment l
                l = l + step;
            }
            // Draw Y lines
            // Set line style for grid
            ctx.strokeStyle = root.gridColor;
            // Stroke the grid
            ctx.stroke();

            /// Draw colliders
            for(var i = 0; i < world.obj_count(); i++) {
                var kind = world.obj_kind(i);
                if (kind == "wall") {
                    var pcount = world.wall_point_count(i);
                    if(pcount > 0) {
                        ctx.beginPath();
                        var pos = world.wall_point(i, 0, root._scale);
                        ctx.moveTo(pos[0], height-pos[1]);

                        for(var p = 1; p < pcount; p++) {
                            pos = world.wall_point(i, p, root._scale);
                            ctx.lineTo(pos[0], height-pos[1]);
                        }

                        ctx.strokeStyle = root.wallColor;
                        ctx.stroke();
                    }
                }
            }

            /// Draw items in world
            /*for(var i = 0; i < world.obj_count(); i++) {
                var kind = world.obj_kind(i);                    
                if(kind == "agent") {
                    var pos = world.obj_position(i, scale);
                    ctx.beginPath();
                    ctx.fillStyle = "#eeaaaaff";
                    ctx.arc(pos[0], height-pos[1], 5, 0, 2*Math.PI);
                    ctx.fill()
                }
                else if (kind == "wall") {
                    var pcount = world.wall_point_count(i);
                    if(pcount > 0) {
                        ctx.beginPath();
                        var pos = world.wall_point(i, 0, scale);
                        ctx.moveTo(pos[0], height-pos[1]);

                        for(var p = 1; p < pcount; p++) {
                            pos = world.wall_point(i, p, scale);
                            ctx.lineTo(pos[0], height-pos[1]);
                        }

                        ctx.strokeStyle = "#ff000000";
                        ctx.stroke();
                    }
                } else if (kind == "item") {
                    var flags = world.item_flags(i);
                    var pos = world.obj_position(i, scale);
                    for (var f = 0; f < flags.length; f++) {
                        var flag = flags[f];
                        if (flag == 0) {
                            // general
                            ctx.beginPath();
                            ctx.fillStyle = "#eeaaaaaa";
                            ctx.arc(pos[0], height-pos[1], 3, 0, 2*Math.PI);
                            ctx.fill()
                        } else if (flag == 1) {
                            // food
                            ctx.beginPath();
                            ctx.fillStyle = "#eeaaffaa";
                            ctx.arc(pos[0], height-pos[1], 3, 0, 2*Math.PI);
                            ctx.fill()
                        }
                    }
                }
            }*/
        }
    }

    // Mouse area to convert pixel coordinates to physical position
    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked(mouseX/root._scale, (height-mouseY)/root._scale)
    }

    // Creates visual representation of items
    Repeater {
        id: objrep
        model: objectlist
        delegate: Rectangle {
            property int area: 0
            property int clickArea: 20
            width: area
            height: area
            x: posX-width/2
            y: parent.height-posY-height/2
            radius: width/2
            MouseArea {
                anchors.centerIn: parent
                width: clickArea
                height: clickArea
                onClicked: {
                    root.inspected(wid);
                }
            }
            Component.onCompleted: {
                if (kind == "agent") {
                    color = Material.color(Material.Blue)
                    area = 12
                } else if (kind == "item") {
                    color = Material.color(Material.Green)
                    area = 8
                } else {
                    color = Material.color(Material.BlueGrey)
                    area = 8
                }
            }
        }
    }

    // List of objects
    ListModel {
        id: objectlist
    }

    // Function to update the list, including updating the
    // position and creating new elements
    function update() {
        // Update items and remove obselated ones
        /*for(var i = 0; i < objectlist.count; i++) {
            var pos = world.wid_pos(objectlist.get(i).wid, root._scale);
            if(pos.length == 0) {
                objectlist.remove(i);
                continue
            }
            var item = objectlist.get(i);
            if (Math.max(Math.abs(item.posX-pos[0]),Math.abs(item.posY-pos[1])) > -1) {
                item.posX = pos[0];
                item.posY = pos[1];
            }
        }
        // Create new items
        var new_wids = world.reset_wid_counter();
        for(var w = 0; w < new_wids.length; w++) {
            if(world.wid_kind(new_wids[w]) == "wall")
                continue
            var pos = world.wid_pos(new_wids[w], root._scale);
            objectlist.append({
                "wid": new_wids[w],
                "kind": world.wid_kind(new_wids[w]),
                "posX": pos[0],
                "posY": pos[1],
            })
        }
        world.reset_wid_counter();*/

        // Signaling finish
        root.updateFinished();
    }
}