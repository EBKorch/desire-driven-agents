use std::f64::{self, consts};
use std::{ops, fmt};

/// # Traits
///
/// The geom2d traits defines some general comparison / transformation
/// which are expected from multiple primitives. The traits are implemented
/// on all primitive type (where it makes sense).
///
/// Currently the following traits are defined:
///
/// ## Transform
///
/// Defines the rotate functions, which rotates the primitive by the defined
/// angle.
///
/// TODO: The following transformations should be considered to
/// implement:
///
/// * scale
/// * mirror
///
/// ## Distance
///
/// This trait only contains the dist method, which meant to calculate the
/// distance between two primitive. The trait is a generic, as different
/// primitives can be compared for the distance.
///
/// ## BezierParameters
///
/// Primitives implementing the bezier parameters should

pub trait Transform {
    fn rotate(&self, angle: f64) -> Self;
}

pub trait Distance<T> {
    fn dist(&self, obj: T) -> f64;
}

pub trait BezierParameters<T> {
    fn bezierparams(&self, thr: T) -> Option<(f64, f64)>;
}

pub trait Intersection<T> {
    fn intersect(&self, thr: T) -> Option<Point>;
}

/// # Point
///
/// Generic type, consists of two coordinates
#[derive(Copy, Clone, PartialEq, Debug)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Point {
    pub fn new<T,U>(x: T, y: U) -> Point where
        T: std::convert::Into<f64>,
        U: std::convert::Into<f64> {
        Point {
            x: x.into(),
            y: y.into(),
        }
    }

    /// Creates the projection vector which moves the
    /// point to the line.
    fn project_vector_to(&self, l: Line) -> Vector {
        // Create a vector peprendicular to the line,
        // with unit length.
        let norm = l.dir.perpend_unit();
        //println!("Norm vec: {}", norm);
        // Calculate the difference vector between the start
        // point of the line, and the point.
        let diff = *self - l.p;
        //println!("Diff vec: {}", diff);
        // Calculate the projection vector to the norm
        let proj_v = norm*(diff*norm);
        //println!("Proj vec: {}", proj_v);
        proj_v
    }

    /// Returns with the projected point to the line
    pub fn project_to(&self, l: Line) -> Point {
        *self + self.project_vector_to(l)
    }
}

impl Transform for Point {
    fn rotate(&self, _angle: f64) -> Self {
        *self
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl ops::Sub for Point {
    type Output = Vector;

    fn sub(self, thr: Point) -> Vector {
        Vector {
            x: self.x-thr.x,
            y: self.y-thr.y,
        }
    }
}

impl ops::Add<Vector> for Point {
    type Output = Point;

    fn add(self, v: Vector) -> Point {
        v + self
    }
}

impl Distance<Line> for Point {
    fn dist(&self, l: Line) -> f64 {
        distance_point_line(*self, l)
    }
}

impl Distance<Point> for Point {
    fn dist(&self, p: Point) -> f64 {
        distance_point_point(*self, p)
    }
}

/// # Vector
///
/// By looking at the definition, it is the same
/// as a Point, however it is to represent a
/// direction with a "weight" - aka a length.
///
/// Vector has a direction and a strting point -
/// both represented by the Point structure.
#[derive(Copy, Clone)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
}

impl Vector {
    /// Creates a new vector from the coordinate
    /// value
    pub fn new<T,U>(x: T, y: U) -> Vector where
        T: std::convert::Into<f64>,
        U: std::convert::Into<f64> {
        Vector {
            x: x.into(),
            y: y.into(),
        }
    }

    /// Creates a new vector from the angle. The
    /// resulting vector is always unit in its
    /// size
    pub fn new_with_angle<T>(angle: T) -> Vector where
        T: std::convert::Into<f64> {
        let angle: f64 = angle.into();
        Vector {
            x: angle.cos(),
            y: angle.sin(),
        }
    }

    /// Creates a vector between two points
    pub fn new_between(from: Point, to: Point) -> Vector {
        Vector {
            x: to.x - from.x,
            y: to.y - from.y,
        }
    }
    /// Returns the length of the vector
    /// Length of the vector is calculated as
    /// the geometric mean of its direction.
    pub fn length(&self) -> f64 {
        ((self.x).powf(2.0) + (self.y).powf(2.0)).sqrt()
    }

    /// Returns with a new vector with unit length
    /// but same direction and starting point.
    pub fn normalize(&self) -> Vector {
        let length = self.length();
        Vector {
            x: self.x/length,
            y: self.y/length,
        }
    }

    /// Returns the angle of vector compared to the unit
    /// vector of direction x.
    pub fn angle(&self) -> f64 {
        self.y.atan2(self.x)
    }

    /// Returns with the angle compared to a specific
    /// vector.
    pub fn angle_to(&self, thr: Vector) -> f64 {
        // If the angle difference is larger than PI
        // - aka 180° - we should return 2*PI - angle
        let diff_angle = (self.angle() - thr.angle()).abs();
        if diff_angle > consts::PI {
            return consts::PI*2.0 - diff_angle
        }
        diff_angle
    }

    /// Returns with the perpendicular vector
    pub fn perpend(&self) -> Vector {
        Vector {
            x: -self.y,
            y: self.x,
        }
    }

    /// Returns the perpendicular unit vector
    pub fn perpend_unit(&self) -> Vector {
        let length = self.length();
        Vector {
            x: -self.y/length,
            y:  self.x/length,
        }
    }

    /// Returns true, if the vector is a null vector
    pub fn is_null(&self) -> bool {
        match *self {
            Vector{x, y} if x==0.0 && y==0.0 => {
                true
            },
            _ => false
        }
    }

    /// Return woth resizes the vector to the desired length
    pub fn resize(&self, length: f64) -> Vector {
        let factor = length/self.length();
        Vector {
            x: self.x*factor,
            y: self.y*factor,
        }
    }

    /// Returns with the component of the vector towards the direction
    pub fn component(&self, dir: &Vector) -> Vector {
        dir*(self*dir)/(dir.length().powf(2.0))
    }
}

impl ops::Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Vector {
        Vector {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl ops::Add for Vector {
    type Output = Vector;

    fn add(self, thr: Vector) -> Vector {
        Vector {
            x: self.x + thr.x,
            y: self.y + thr.y,
        }
    }
}

impl ops::Sub for Vector {
    type Output = Vector;

    fn sub(self, thr: Vector) -> Vector {
        Vector {
            x: self.x - thr.x,
            y: self.y - thr.y,
        }
    }
}

impl ops::Add<Point> for Vector {
    type Output = Point;

    fn add(self, thr: Point) -> Point {
        Point {
            x: thr.x + self.x,
            y: thr.y + self.y,
        }
    }
}

impl ops::Mul for Vector {
    type Output = f64;

    fn mul(self, thr: Vector) -> f64 {
        self.x*thr.x+self.y*thr.y
    }
}

impl ops::Mul<&Vector> for &Vector {
    type Output = f64;

    fn mul(self, thr: &Vector) -> f64 {
        self.x*thr.x+self.y*thr.y
    }
}

impl<T: std::convert::Into<f64> + Copy> ops::Div<T> for Vector {
    type Output = Vector;

    fn div(self, scalar: T) -> Vector {
        Vector {
            x: self.x/scalar.into(),
            y: self.y/scalar.into(),
        }
    }
}

impl<T: std::convert::Into<f64> + Copy> ops::Mul<T> for Vector {
    type Output = Vector;

    fn mul(self, thr: T) -> Vector {
        Vector {
            x: self.x*thr.into(),
            y: self.y*thr.into(),
        }
    }
}

impl<T: std::convert::Into<f64> + Copy> ops::Mul<T> for &Vector {
    type Output = Vector;

    fn mul(self, thr: T) -> Vector {
        Vector {
            x: self.x*thr.into(),
            y: self.y*thr.into(),
        }
    }
}

impl Transform for Vector {
    fn rotate(&self, angle: f64) -> Self {
        let a_sin = angle.sin();
        let a_cos = angle.cos();
        Vector {
            x: self.x*a_cos - self.y*a_sin,
            y: self.x*a_sin + self.y*a_cos,
        }
    }
}

impl fmt::Display for Vector {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}]", self.x, self.y)
    }
}

/// # Line
///
/// Line is an infinite primitive, which can
/// be given in multiple forms. The implementation
/// uses a start point, and a direction.
#[derive(Copy, Clone)]
pub struct Line {
    pub p: Point,
    pub dir: Vector,
}

impl Line {
    /// Creates a line from four input arguments:
    ///
    /// - x coordinate of the point
    /// - y coordinate of the point
    /// - x coordinate of the direction vector
    /// - y coordinate of the direction vector
    pub fn new<T,U,V,W>(x: T, y: U, dx: V, dy: W) -> Line where
        T: std::convert::Into<f64>,
        U: std::convert::Into<f64>,
        V: std::convert::Into<f64>,
        W: std::convert::Into<f64> {
        let p = Point {
            x: x.into(),
            y: y.into(),
        };
        let dir = Vector {
            x: dx.into(),
            y: dy.into(),
        };
        Line {p, dir}
    }

    /// Create a line from the coordinates of the
    /// starting point, and the angle for the direction
    /// vector.
    pub fn new_with_angle<T,U,V>(x: T, y: U, angle: V) -> Line where
        T: std::convert::Into<f64>,
        U: std::convert::Into<f64>,
        V: std::convert::Into<f64> {
        let dir = Vector::new_with_angle(angle);
        let p = Point {
            x: x.into(),
            y: y.into(),
        };
        Line {p, dir}
    }
}

impl fmt::Display for Line {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Line {} -> {}", self.p, self.dir)
    }
}

impl From<LineSegment> for Line {
    fn from(ls: LineSegment) -> Self {
        Line {
            p: ls.p1,
            dir: ls.p2-ls.p1,
        }
    }
}

impl Distance<Line> for Line {
    fn dist(&self, line: Line) -> f64 {
        distance_line_line(*self, line)
    }
}

impl Distance<Point> for Line {
    fn dist(&self, point: Point) -> f64 {
        distance_point_line(point, *self)
    }
}

impl BezierParameters<Line> for Line {
    fn bezierparams(&self, line: Line) -> Option<(f64,f64)> {
        bezier_line_line(*self, line)
    }
}

impl Intersection<Line> for Line {
    fn intersect(&self, line: Line) -> Option<Point> {
        intersect_line_line(*self, line)
    }
}

/// # Line segment
///
/// Line segment is like a line, but not infinite
/// long: it is between two points. The implementation
/// reflects this, the segment consist of two points
#[derive(Copy, Clone)]
pub struct LineSegment {
    pub p1: Point,
    pub p2: Point,
}

impl LineSegment {
    /// Constructs a line segment from the coordinates
    /// of the two points.
    pub fn new<T,U,V,W>(x1: T, y1: U, x2: V, y2: W) -> Self where
        T: std::convert::Into<f64>,
        U: std::convert::Into<f64>,
        V: std::convert::Into<f64>,
        W: std::convert::Into<f64> {
        LineSegment {
            p1: Point::new(x1,y1),
            p2: Point::new(x2,y2),
        }
    }

    /// Constructs a line segment from a starting point
    /// and a vector. The end of the line segment is the
    /// point at starting point + vector.
    pub fn new_from_vec(start: Point, vec: Vector) -> Self {
        let end = start + vec;
        LineSegment {
            p1: start,
            p2: end,
        }
    }

    /// Returns with a unique length direction vector
    pub fn dir(&self) -> Vector {
        (self.p2 - self.p1).normalize()
    }

    /// Resizes the segment from the geometric center
    pub fn add_length(&self, amount: f64) -> LineSegment {
        let dir = (self.p2-self.p1).resize(amount/2.0);
        LineSegment {
            p1: self.p1+(-dir),
            p2: self.p2+dir,
        }
    }
}

impl Distance<Point> for LineSegment {
    fn dist(&self, point: Point) -> f64 {
        distance_linesegment_point(*self, point)
    }
}

impl Distance<Line> for LineSegment {
    fn dist(&self, line: Line) -> f64 {
        distance_linesegment_line(*self, line)
    }
}

impl Distance<LineSegment> for LineSegment {
    fn dist(&self, ls: LineSegment) -> f64 {
        distance_linesegment_linesegment(*self, ls)
    }
}

impl Intersection<Line> for LineSegment {
    fn intersect(&self, line: Line) -> Option<Point> {
        intersect_linesegment_line(*self, line)
    }
}

impl Intersection<LineSegment> for LineSegment {
    fn intersect(&self, ls: LineSegment) -> Option<Point> {
        intersect_linesegment_linesegment(*self, ls)
    }
}

impl BezierParameters<LineSegment> for LineSegment {
    fn bezierparams(&self, ls: LineSegment) -> Option<(f64, f64)> {
        bezier_from_points(self.p1, self.p2, ls.p1, ls.p2)
    }
}

/// # Distances
///
/// Various functions for calculating the distance
/// between different primitives. These are private
/// functions, as for external use teh Distance
/// trait is provided.

/// ## Point-point distance
///
/// The distance between two point is the length of
/// the difference vector.
#[inline(always)]
fn distance_point_point(p1: Point, p2: Point) -> f64 {
    (p2-p1).length()
}

/// ## Line-point distance
///
/// It is equivalent to the length of the projection
/// vector for point -> line
#[inline(always)]
fn distance_point_line(p: Point, l: Line) -> f64 {
    // Get the projection vector
    let proj_vec = p.project_vector_to(l);
    // The length of this vector is the distance
    proj_vec.length()
}

//// ## Line-line distance
#[inline(always)]
fn distance_line_line(l1: Line, l2: Line) -> f64 {
    // We have to check if the two lines do not intersect.
    // This can be done by ensuring that the two direction
    // vector is parallel.
    let angle = l1.dir.angle_to(l2.dir);
    if angle.abs() > f64::EPSILON {
        return 0.0
    }
    // If the two line is prallel, we can take a point on
    // one, and calculate the distance of this point to
    // other line.
    distance_point_line(l1.p, l2)
}

/// ## Line segment - point distance
#[inline(always)]
fn distance_linesegment_point(ls: LineSegment, p: Point) -> f64 {
    // Check if the projection of the point is on the line segment
    let proj_p = p.project_to(Line::from(ls));
    //println!("Projected: {}, angle: {}", proj_p, (ls.p1-proj_p).angle_to(ls.p2-proj_p));
    //println!("Vectors: {}, {}", ls.p1-proj_p, ls.p2-proj_p);
    if (ls.p1-proj_p).angle_to(ls.p2-proj_p) != consts::PI {
        distance_point_point(proj_p, p)
    } else {
        (ls.p1-p).length().min((ls.p2-p).length())
    }
}

/// ## Line segment - line distance
#[inline(always)]
fn distance_linesegment_line(ls: LineSegment, l: Line) -> f64 {
    // Create the projection vectors for both points of
    // the line segment - if they have opposite directions,
    // than the line segment intersects the line, and the
    // distance is zero.
    let proj_v1 = ls.p1.project_vector_to(l);
    let proj_v2 = ls.p2.project_vector_to(l);
    println!("PV1: {}", proj_v1.angle_to(proj_v2)/consts::PI);
    // If either the projection vectors is null vector, that
    // the starting or ending point is on the line, distance
    // is zero.
    if proj_v1.is_null() || proj_v2.is_null() {
        return 0.0
    }
    // If the summary of the two normalized projecion vector
    // is zero, the line segment crosses the line.
    if proj_v1.angle_to(proj_v2) == consts::PI {
        return 0.0
    }
    // If we managed to reach this point, the line does not
    // crosses the line segment, the distance is the minimum
    // from the distances of the starting and ending point
    // of the line segment from the line. We have the original
    // projection vectors, so the length can be compared.
    let dist = proj_v1.length().min(proj_v2.length());
    dist
}

/// ## Line segment - line segment distance
#[inline(always)]
fn distance_linesegment_linesegment(ls1: LineSegment, ls2: LineSegment) -> f64 {
    let dist = match ls1.bezierparams(ls2) {
        None => distance_line_line(Line::from(ls1), Line::from(ls2)),
        Some((t,u)) => {
            if u >= 0.0 && u <= 1.0 && t >= 0.0 && t <= 1.0 {
                // The two segment intersects!
                println!("Intersection! {}, {}", u, t);
                0.0
            } else {
                // The two line do not intersect, we have to check
                // the distances of points of the segments compared
                // to the other segment, and the minimum is the real
                // distance
                distance_linesegment_point(ls1, ls2.p1)
                    .min(distance_linesegment_point(ls1, ls2.p2))
                    .min(distance_linesegment_point(ls2, ls1.p1))
                    .min(distance_linesegment_point(ls2, ls1.p2))
            }
        },
    };
    dist
}

/// # Bezier parameters
///
/// General functions for bezier parameter calculations

/// ## Bezier parameter values from four point
#[inline(always)]
fn bezier_from_points(p10: Point, p11: Point, p20: Point, p21: Point) -> Option<(f64,f64)> {
    // Extract parameters for easier annotation
    let (x1, y1) = (p10.x, p10.y);
    let (x2, y2) = (p11.x, p11.y);
    let (x3, y3) = (p20.x, p20.y);
    let (x4, y4) = (p21.x, p21.y);

    // Calculate the determinant - if it is zero, the primitives do
    // not intersect.
    let det = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if det == 0.0 {
        // Check if all point is on a common line
        return None
    }

    // Calculate the two bezier parameter
    let t = ((x1-x3)*(y3-y4) - (y1-y3)*(x3-x4)) / det;
    let u = ((x1-x2)*(y1-y3) - (y1-y2)*(x1-x3)) / det;

    // Return
    Some((t,u))
}

#[inline(always)]
fn bezier_line_line(l1: Line, l2: Line) -> Option<(f64,f64)> {
    // Create two dummy point, one on both lines
    let p1 = l1.p + l1.dir;
    let p2 = l2.p + l2.dir;

    // Calculate the return value from the general bezier function
    bezier_from_points(l1.p, p1, l2.p, p2)
}

/// # Intersection functions
///
/// Private functions for returning the intersection points

/// ## Line-Line intersection
#[inline(always)]
fn intersect_line_line(l1: Line, l2: Line) -> Option<Point> {
    // Calculate bezier parameters
    match l1.bezierparams(l2) {
        Some((t,_)) => Some(l1.p + l1.dir*t),
        None => None,
    }
}

// ## Linesegment - linesegment intersection
#[inline(always)]
fn intersect_linesegment_linesegment(ls1: LineSegment, ls2: LineSegment) -> Option<Point> {
    // Retrieve the bezire parmeters
    match ls1.bezierparams(ls2) {
        Some((t,u)) if t >= 0.0 && t <= 1.0 && u >= 0.0 && u <= 1.0 =>
            Some(ls1.p1 + (ls1.p2-ls1.p1)*t),
        _ => None,
    }
}

// ## Linesegment - line intersection
#[inline(always)]
fn intersect_linesegment_line(ls: LineSegment, line: Line) -> Option<Point> {
    // Check if the Bezier parameter 'l' indicates that
    // the intresection point is on the line
    match Line::from(ls).bezierparams(line) {
        Some((t,_)) if t >= 0.0 && t <= 1.0 =>
            Some(ls.p1 + (ls.p2-ls.p1)*t),
        _ => None,
    }
}
