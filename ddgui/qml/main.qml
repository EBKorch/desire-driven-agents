import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.12

import World 1.0

// Main QML, composes the full window
//
// Uses a top panel for navigation, a side panel for detailed settings,
// and a large area where the actual simulation is drawn.
ApplicationWindow {
    id: window
    visible: true
    width: 800
    height: 480
    title: "Desire Driven Agents Simulator"

    Material.theme: Material.Light

    // Store tha state of the side panel in a separate
    // variable to be able to restore it when hiding / unnhiding
    property string lastSideState: ""
    // This variable stores wheter or not the GUI in such a state, that items
    // can be added to the World. If false, clicks will not be processe
    // by the adding mechanism
    property bool acceptPoints: false

    // Time resolution of simulation in seconds
    property real simulationTimeStep: 0.1

    Item {
        anchors {
            top: toppanel.bottom
            left: sidepanel.right
            right: parent.right
            bottom: parent.bottom
            margins: 2
        }

        // Custom QML type which is responsible for displaying the items and
        // agents. It also provides signals to catch events when a specific
        // object is clicked.
        WorldView {
            id: worldview
            anchors.fill: parent
            model: world.objectList
            timeStep: simulationTimeStep
            timeResolution: world.modelUpdateFrequency

            // Add objects to the world (when the GUI is in such a state) under
            // the cursor.
            onClicked: {
                if (acceptPoints) {
                    // Convert the pixel coordinates to physical ones
                    var x = worldview.toPhysicalX(mouseX);
                    var y = worldview.toPhysicalY(mouseY);
                    // Branch depending on what kind of object is selected
                    switch (sidepanel.targetObject) {
                        case "Agent":
                            world.addAgent(x, y);
                            break;
                        case "Food":
                            world.addItem(x, y);
                            break;
                    }
                }
            }

            // When an item is clicked, request an update on that item's
            // information from the world instance.
            onItemClicked: {
                world.updateAgentInfo(key);
            }
        }
    }

    // Custom QML type to create a side panel where the user can add objects
    // and can retrieve the information of Agents.
    SidePanel {
        id: sidepanel
        anchors {
            top: toppanel.bottom
            left: parent.left
            bottom: parent.bottom
        }
        width: 300

        // The Inspector section of the side panel requires two information:
        //   - the agent information custom Qt type (implemented by the World)
        //   - the id of the information type
        agentInfo: world.agentInfo
        activeInfo: world.activeInfo

        onHideClicked: toppanel.unselect_all();

        // When the add object button is clicked - round button with "+" sign - the
        // GUI will accept points. When in this state the user clickes on the
        // WorldView a new object will be added to the world.
        onAddObjectClicked: {
            if (acceptPoints) {
                acceptPoints = false;
            } else {
                acceptPoints = true;
            }
        }

        onCurrentPageChanged: {
            // If we change to the inspector page, the timer controlling
            // the inspector update should be started. Otherwise stop
            // this timer if it is running.
            if (currentPage == 0) {
                inspectortimer.start();
            } else if (inspectortimer.running) {
                inspectortimer.stop();
            }
        }
    }

    // Custom QML type implementing a top panel where shortcuts and
    // categories (including the SidePanel menus) are available.
    TopPanel {
        id: toppanel
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: 35

        // The top panel provides the hide button, which when clicked, the
        // top panel as well as the side panel will be hidden. When this happens,
        // we want to save the state of the side panel, so when we unhide both,
        // the side panel will return to the same menu.
        onHideClicked: {
            if(state == "closed") {
                sidepanel.state = lastSideState;
                if (sidepanel.state == "") select("addbutton")
                else if (sidepanel.state != "closed") select(sidepanel.state)
            } else {
                lastSideState = sidepanel.state
                sidepanel.state = "closed"
            }
        }

        // Switch the color theme of the main window when the DarkMode button is
        // clicked.
        onDarkModeClicked: {
            window.Material.theme = window.Material.theme == Material.Dark ? Material.Light : Material.Dark
        }

        // Switch the side panel to the add state, or close it when the add
        // menu is already selected. This logic applies to the following fuctions
        // as well
        onAddClicked: {
            if (sidepanel.state == "") {
                sidepanel.state = "closed";
                unselect_all();
            } else {
                sidepanel.state = "";
                unselect_all();
                select("addbutton");
                sidepanel.currentPage = 1;
            }
        }
        onInspectorClicked: {
            if (sidepanel.state == "inspector") {
                sidepanel.state = "closed";
                unselect_all();
            } else {
                sidepanel.state = "inspector";
                unselect_all();
                select("inspector")
                sidepanel.currentPage = 0;
            }
        }

        // Start the timer which steps the simulation when the AutoStep button
        // is clicked.
        onAutoStepClicked: {
            if (checked) steptimer.start();
            else steptimer.stop();
        }

        onStepClicked: {
            world.step(steptimer.interval/1000);
            // Update the agent information as well if an one is
            // already selected
            if (sidepanel.activeKey != undefined) {
                world.updateAgentInfo(sidepanel.activeKey);
            }
        }
    }

    // Custom type implemented in Rust. Contains every logic which can
    // be indepented of the GUI, as well as some custom Qt types for
    // easier communication between the GUI and the logic.
    World {
        id: world
        // This controls how often the GUI is updated compared to the
        // simulation step.
        modelUpdateFrequency: 5
    }

    // Timer for automatic simulation stepping.
    Timer {
        id: steptimer
        // Interval is in milisec
        interval: simulationTimeStep*1000
        repeat: true
        onTriggered: {
            // World expects the time delta to be in seconds,
            // while the interval of the timer is in milisecs
            world.step(simulationTimeStep);
        }
    }

    // Timer responsible for updating the inspector. 
    Timer {
        id: inspectortimer
        interval: 500
        repeat: true
        onTriggered: {
            // Only update the information if:
            //   - we already selected an agent, and
            //   - the step timer is running (aka only update when there is
            //     new information)
            if (sidepanel.activeKey != undefined && steptimer.running) {
                world.updateAgentInfo(sidepanel.activeKey);
            }
        }
    }
}