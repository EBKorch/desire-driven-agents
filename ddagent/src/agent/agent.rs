use super::{
    actions::{ Action, ActionData },
    hormon::{Hormon, HormonData, StaticChange },
    goal::{ Goal, GoalData, Connection },
    item::{Items, Item, Flag },
    requirement::{ Requirement, Event, Target },
    effect::{ Effect, Change },
};

use std::collections::{
    HashMap,
};

use itertools::Itertools;

/// The thinking entity itself. You should create an instace of
/// it to to start using the Agent.
///
/// The Agent is usable by itself, but you should probably create
/// a wrapper for your specific use case. The wrapper should be
/// able to:
///     - pass Item instances to the Agent
///     - pass Reaction instances to the Agent
/// The wrapper should also recognize the Actions coming from the
/// Agent, and make them happen (step when when the Step action is
/// recieved, etc.).
///
/// # Fields
/// 
/// The Agent structure has three main fields.
///
/// ## 1. Knowledgebase
///
/// Contains an abstract representation of the Agent abilities (actions),
/// and of the world (items).
/// 
/// ## 2. Bloodstream
/// 
/// The list of 'Hormons' which the Agent has. Hormons are the basis of
/// decision making, and responds to effects from the world. They also
/// change on their own.
///
/// ## 3. Goals 
///
/// This is the part of the Agent where we can influence the behaviort
/// and set the target we want to achive with the Agent. These are 'soft'
/// targets, which means we 'ask' the Agent to do them, but the time
/// when the execution of them finished is unknow.
/// 
/// This structure means that we only need to define the targets, and the
/// Agent will schedule it, and will plan an execution order.
pub struct Agent {
    selfitem: Item,
    actions: HashMap<String, ActionData>,
    items: Items,
    bloodstream: HashMap<String, HormonData>,
    goals: HashMap<String, GoalData>,
    inventory: Option<Item>,
    status: Status,
    event_history: Vec<Event>,
}

impl Agent {
    pub fn new(selfitem: Item) -> Self {
        let actions = HashMap::new();
        let items = Items::default();
        let bloodstream = HashMap::new();
        let goals = HashMap::new();
        let inventory = None;
        let event_history = Vec::new();
        Agent { selfitem, actions, items, bloodstream, goals, inventory, status: Status::Idle, event_history }
    }

    // # Define the behavior of the Agent
    /// Add goals to the Agent. 
    pub fn add_goals(&mut self, goals: Vec<Goal>) {
        // Add default hormons to new names
        for hormon in goals.iter().map(|g| g.data.connections.iter().map(|c| &c.hormon)).flatten() {
            if let None = self.bloodstream.get(hormon) {
                self.bloodstream.insert(hormon.clone(), HormonData::default());
            }
        }
        // Add the goals
        for goal in goals {
            self.goals.insert(goal.name, goal.data);
        }
    }
    /// Add hormons
    pub fn add_hormons(&mut self, hormons: Vec<Hormon>) {
        for hormon in hormons {
            self.bloodstream.insert(hormon.name, hormon.data);
        }
    }
    /// Add actions
    pub fn add_actions(&mut self, actions: Vec<Action>) {
        for action in actions {
            self.actions.insert(action.name, action.data);
        }
    }

    // # Actions
    /// Create all available action
    ///
    /// Creates the list of all available actions and with the index of
    /// the target. If the target is None, than the action can be done
    /// by the Agent on its own.
    pub fn list_available_events(&self) -> Vec<Event> {
        // This list will be populated
        let mut available = Vec::new();
        // First select actions without targets
        for (name, data) in &self.actions {
            if !data.has_target {
                available.push(Event::new(name.clone(), None));
            }
        }
        // Now list actions from Items
        for (id, item) in self.items.get_all() {
            for (name, data) in &item.accepts {
                available.push(Event::new(name.clone(), Some(Target::Item(*id))));
            }
        }
        // Check the inventory
        if let Some(item) = &self.inventory {
            for (name, data) in &item.accepts {
                available.push(Event::new(name.clone(), Some(Target::Inventory())));
            }
        }
        available
    }
    pub fn list_available_events_sorted(&self, goal_name: &String) -> Vec<Event> {
        let mut events = self.list_available_events();
        for event in &mut events {
            event.profit = self.calc_event_profit(event, goal_name);
        }
        events = events.iter().filter_map(|event| {
            match event.profit {
                Some(value) if value > 0.0 => Some(event.clone()),
                _ => None,
            }
        }).collect();
        events.sort_by(|a,b| a.profit.unwrap().partial_cmp(&b.profit.unwrap()).unwrap());
        events.reverse();
        events
    }

    // # Events
    // As an Event is a quite light structure, it does not contain
    // every information required to be useful on its own. Therefore
    // corresponding functions are implemented inside the Agen structure
    /// Calculates the profit for the Event
    pub fn calc_event_profit(&self, event: &Event, goal_name: &String) -> Option<f64> {
        let mut profit = None;
        if let Some(goal_data) = self.goals.get(goal_name) {
            let current_importance = goal_data.get_calculated_importance(&self.bloodstream);
            let mut effects = Vec::new();
            let duration = match self.actions.get(&event.action_name) {
                Some(action_data) => {
                    match &event.target {
                        Some(Target::Item(id)) => {
                            if let Some(item) = self.get_item(id) {
                                println!("Item from the world");
                                effects.append(&mut action_data.get_effect_with_target(&item, &self.selfitem));
                                effects.append(&mut item.get_effects(&event.action_name, &self.selfitem));
                            }
                        }
                        Some(Target::Inventory()) => {
                            if let Some(item) = &self.inventory {
                                println!("Item from inventory");
                                effects.append(&mut action_data.get_effect_with_target(&self.selfitem, &self.selfitem));
                                effects.append(&mut item.get_effects(&event.action_name, &self.selfitem));
                            }
                        }
                        None if !action_data.has_target => {
                            println!("Action on its own: {}", event.action_name);
                            effects.append(&mut action_data.get_effect_with_target(&self.selfitem, &self.selfitem));
                        }
                        _ => return profit,
                    }
                    action_data.duration.clone()
                }
                None => {
                    println!("No such action in agent: {}", event.action_name);
                    return profit
                }
            };
            let mut new_bloodstream = self.bloodstream.clone();
            for effect in effects {
                if let Some(hormon) = new_bloodstream.get_mut(&effect.hormon_name) {
                    hormon.apply_change(&effect.change)
                }
            }
            if let Some(time) = &duration {
                for (_, hormon) in &mut new_bloodstream {
                    hormon.selfchange(time);
                }
            }
            let new_importance = goal_data.get_calculated_importance(&new_bloodstream);
            profit = Some(current_importance-new_importance);
        }
        profit
    }
    /// Returns with requirements of the event
    pub fn get_event_requirement(&self, event: &Event) -> Vec<Requirement> {
        let mut requirements = Vec::new();
        match &event.target {
            Some(Target::Item(id)) => {
                if let Some(item) = &self.get_item(id) {
                    if let Some(action_data) = item.accepts.get(&event.action_name) {
                        requirements.append(&mut action_data.requirements.clone());
                    }
                }
            }
            Some(Target::Inventory()) => {
                if let Some(item) = &self.inventory {
                    if let Some(action_data) = item.accepts.get(&event.action_name) {
                        requirements.append(&mut action_data.requirements.clone());
                    }
                }
            }
            _ => (),
        }
        if let Some(action_data) = self.actions.get(&event.action_name) {
            requirements.append(&mut action_data.requirements.clone());
        }
        requirements
    }
    /// Returns with the resolving events
    pub fn resolve_requirement(&self, requirements: &Vec<Requirement>, target: &Option<Target>) -> Vec<Vec<Event>> {
        let mut events = Vec::new();
        for requirement in requirements {
            events.append(&mut self.resolve_single_requirement(requirement, target));
        }
        let len = events.len();
        events.into_iter().permutations(len).collect()
    }
    fn item_good_for_requirement(&self, item: &Item, req_item: &Item) -> bool {
        let mut good = true;
        for (flag_name, req_flag) in &req_item.flags {
            if let Some(flag) = item.flags.get(flag_name) {
                match flag {
                    Flag::NumLike(known_value) => {
                        if let Flag::NumLike(req_value) = req_flag {
                            if known_value == req_value {
                                continue;
                            }
                        }
                    }
                    Flag::StrLike(know_value) => {
                        if let Flag::StrLike(req_value) = req_flag {
                            if know_value == req_value {
                                continue;
                            }
                        }
                    }
                }
            }
            good = false;
            break;
        }
        good
    }
    fn resolve_single_requirement(&self, requirement: &Requirement, target: &Option<Target>) -> Vec<Event> {
        match requirement {
            Requirement::Action(event) => {
                if let Some(action_data) = self.actions.get(&event.action_name) {
                    if action_data.has_target {
                        if event.action_name == String::from("MoveTo") {
                            match target {
                                Some(Target::Item(id)) => {
                                    if let Some(item) = self.get_item(id) {
                                        if self.selfitem.state.is_some() && item.state.is_some() {
                                            let selfpos = self.selfitem.state.as_ref().unwrap().position;
                                            let itempos = item.state.as_ref().unwrap().position;
                                            if (selfpos - itempos).length() < 0.1 { return Vec::new() }
                                        }
                                    }
                                }
                                Some(Target::Inventory()) => {
                                    return Vec::new()
                                }
                                _ => ()
                            }
                        }
                        vec![Event::new(event.action_name.clone(), target.clone())]
                    } else {
                        vec![Event::new(event.action_name.clone(), None)]
                    }
                } else {
                    vec![Event::new(event.action_name.clone(), None)]
                }
            }
            Requirement::Item((item, action_name)) => {
                let mut item_events = Vec::new();
                if let Some(inventory_item) = &self.inventory {
                    if self.item_good_for_requirement(inventory_item, item) {
                        return Vec::new()
                    }
                }
                for (id, known_item) in self.items.get_all() {                    
                    if self.item_good_for_requirement(&known_item, item) {
                        item_events.push(Event::new(action_name.clone(), Some(Target::Item(*id))));
                    }
                }
                item_events
            }
            _ => unreachable!("This requirement is not implemented yet. :(")
        }
    }

    // # Event tree
    /// Generates the event tree for the specified Goal
    pub fn generate_event_tree(&mut self) -> Vec<Vec<Event>> {
        // The empty eventree
        let mut tree = Vec::new();
        // Select the goal
        let goal_name = match self.eval_goals() {
            Some(name) => name,
            None => return tree,
        };
        // Get the starting events
        let events = self.list_available_events_sorted(&goal_name);
        // Append the starting events to the tree
        for event in &events {
            tree.push(vec![event.clone()])
        }
        // Run tree generation until finished (10 round maximum)
        for _ in 0..10 {
            let mut new_tree = Vec::new();
            for branch in &tree {
                if branch.len() < 1 { continue }
                let last_event = &branch[branch.len()-1];
                let last_requirements = self.get_event_requirement(&last_event);
                if last_requirements.len() < 1 {
                    println!("No new requirement");
                    new_tree.push(branch.clone());
                    continue;
                }
                let mut new_events = self.resolve_requirement(&last_requirements, &last_event.target);
                if new_events.len() < 1 { continue }
                for e in &mut new_events {
                    for event in e {
                        event.profit = self.calc_event_profit(&event, &goal_name);
                    }
                }
                for new_branch in &mut new_events {
                    println!("Making the branch longer...");
                    let mut concatenated = branch.clone();
                    concatenated.append(new_branch);
                    new_tree.push(concatenated);
                }
            }
            let mut no_change = true;
            if tree.len() == new_tree.len() {
                for (index, branch) in tree.iter().enumerate() {
                    if new_tree[index].len() != branch.len() {
                        no_change = false;
                        break;
                    }
                }
            } else { no_change = false }
            if no_change {
                break;
            }
            tree = new_tree;
            println!("Tree:");
            for branch in &tree {
                print!("Branch: ");
                for event in branch {
                    print!("{} ({}) -> ", event.action_name, event.profit.unwrap_or_else(|| -1.0));
                }
                print!("\n");
            }
            println!("");
        }        
        // Return with the event tree
        tree
    }
    // Select best branch
    pub fn best_branch_from_tree(&self, tree: &Vec<Vec<Event>>) -> Vec<Event> {
        // Calculate the profit for every branch
        let mut branch_profts: Vec<(usize, f64)> = tree.iter().enumerate().map(|(i, branch)| {
            (i, branch.iter().map(|event| event.profit.unwrap_or_else(|| 0.0)).sum())
        }).collect();
        for profit in &branch_profts {
            println!("Profit: {}", profit.1);
        }
        branch_profts.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        branch_profts.reverse();
        tree[branch_profts[0].0].clone()
    }
    // Select next action
    pub fn select_and_set_event(&mut self) {
        let tree = self.generate_event_tree();
        let branch = self.best_branch_from_tree(&tree);
        if branch.len() > 0 {
            let event = branch[branch.len()-1].clone();
            self.status = Status::WaitingEvent(event);
        }
    }

    // # Getters
    /// Get hormon level
    pub fn get_hormon_level(&self, hormon_name: &String) -> Option<f64> {
        if let Some(hormon) = self.bloodstream.get(hormon_name) {
            Some(hormon.get_level())
        } else {
            None
        }
    }
    /// Get Goal priority
    pub fn get_goal_importance(&self, goal_name: &String) -> Option<f64> {
        if let Some(goal) = self.goals.get(goal_name) {
            Some(goal.importance)
        } else {
            None
        }
    }

    // # Altering items
    /// Insert items when detected
    pub fn insert_item(&mut self, item: Item, id: Option<&usize>) -> Option<usize> {
        self.items.insert_item(item, id)
    }
    /// Update detected items
    pub fn update_item(&mut self, item: Item, id: &usize) -> bool {
        self.items.update_item(item, id)
    }
    /// Release items no longer tracked
    pub fn release_item(&mut self, id: &usize) -> bool {
        self.items.release_item(id)
    }
    /// Get an item by its ID
    pub fn get_item(&self, id: &usize) -> Option<Item> {
        self.items.get(id)
    }
    /// Add item to inventory
    pub fn add_item_to_inventory(&mut self, item: Item) {
        self.inventory = Some(item);
    }
    pub fn empty_inventory(&mut self) -> Option<Item> {
        let item = self.inventory.clone();
        self.inventory = None;
        item
    }

    // # Effects of reactions
    /// Apply a list of reactions
    pub fn apply_effects(&mut self, effects: &Vec<Effect>) {
        for effect in effects {
            if let Some(hormon) = self.bloodstream.get_mut(&effect.hormon_name) {
                hormon.apply_change(&effect.change)
            }
        }
    }

    // # Controlling the body
    /// Make an action. The machine is expected to start executing this action
    /// by reading the Agent status.
    pub fn take_action(&mut self, action_name: String, target: usize) {
        
    }
    /// Give the results of an executed action
    pub fn action_finished(&mut self, action_name: String, successfull: bool, effects: &Vec<Effect>) {
        self.apply_effects(effects);
        self.status = Status::Idle;
    }

    // # Behavior
    /// Calculate the importance of all goals, returns with the name
    /// of top priority
    pub fn eval_goals(&mut self) -> Option<String> {
        // Calculate the urgancy of goals
        for (_, goal) in self.goals.iter_mut() {
            goal.calc_importance(&self.bloodstream);
        }
        // Create a vector from goals and sort them by importance
        let mut goalvec: Vec<(&String, &GoalData)> = self.goals.iter().collect();
        goalvec.sort_by(|a, b|
            a.1.importance.partial_cmp(&b.1.importance).unwrap()
        );
        // Reverse the vector to get the highest priority in front
        goalvec.reverse();
        // Return with the name of the most important goal
        if goalvec.len() > 0 {
            Some(goalvec[0].0.clone())
        } else {
            None
        }
    }
}

pub enum Status {
    Idle,
    Busy,
    WaitingEvent(Event),
}

#[cfg(test)]
mod tests {
    use super::{
        Agent, Status,
        Hormon, HormonData, StaticChange,
        Goal, GoalData, Connection,
        Action, ActionData,
        Effect, Requirement, Change, Event,
        Item, Target,
    };
    use crate::agent::{
        representation::{ Shape, State },
        item::{ FlagUnit, Flag },
        effect::DynamicChange,
    };
    use geom2d::{ Point, Vector, LineSegment };

    fn create_test_agent() -> Agent {
        // Create the empty agent
        let mut agent = Agent::new(Item::new(
            Some(State::default()),
            Some(Shape::default()),
            Vec::new(),
            Vec::new(),
        ));
        // Add the Hormons to the agent. Only Hunger for now
        agent.add_hormons(vec![
            Hormon {
                name: String::from("Hunger"),
                data: HormonData::new(5.0, StaticChange::Linear(1.0))
            },
            Hormon {
                name: String::from("Chill"),
                data: HormonData::new(6.0, StaticChange::Disabled)
            }
        ]);
        // Add Goals to the agent - only KeepHungerLow for now
        agent.add_goals(vec![
            Goal {
                name: String::from("KeepHungerLow"),
                data: GoalData::new(vec![
                    Connection { hormon: String::from("Hunger"), value: 1.5 }
                ])
            },
            Goal {
                name: String::from("Explore"),
                data: GoalData::new(vec![
                    Connection { hormon: String::from("Hunger"), value: -0.1}
                ])
            }
        ]);
        // Add Actions to the agent - Eat and Move
        agent.add_actions(vec![
            Action::new(
                String::from("Eat"),
                ActionData::new(
                    vec![
                        Requirement::Action(Event::new(
                            String::from("MoveTo"),
                            None,
                        ))
                    ],
                    vec![
                        Effect::new(String::from("Hunger"), Change::Absolute(-2.5))
                    ],
                    Some(2.0),
                    true,
                    0,
                )
            ),
            Action::new(
                String::from("Search"),
                ActionData::new(
                    Vec::new(),
                    Vec::new(),
                    None,
                    false,
                    0,
                )
            ),
            Action::new(
                String::from("MoveTo"),
                ActionData::new(
                    Vec::new(),
                    vec![
                        Effect::new(
                            String::from("Hunger"),
                            Change::Dynamic(DynamicChange::Position(0.02)),
                        )
                    ],
                    None,
                    true,
                    0,
                )
            )
        ]);
        agent
    }

    fn create_test_item(position: Point) -> Item {
        Item::new(
            Some(State::new(position, Vector::new(0, 0))),
            Some(Shape::default()),
            vec![FlagUnit::new(String::from("Nutrition"), Flag::NumLike(1.0))],
            vec![
                Action::new(String::from("Eat"), ActionData::new(
                    Vec::new(),
                    vec![
                        Effect::new(String::from("Hunger"), Change::Absolute(-3.5)),
                    ],
                    Some(1.25),
                    true,
                    0
                )),
                Action::new(String::from("Destroy"), ActionData::new(
                    vec![
                        Requirement::Action(Event::new(String::from("MoveTo"), None)),
                    ],
                    vec![
                        Effect::new(String::from("Hunger"), Change::Absolute(4.0))
                    ],
                    Some(0.5),
                    true,
                    0,
                )),
                Action::new(String::from("MoveTo"), ActionData::new(
                    Vec::new(),
                    Vec::new(),
                    None,
                    true,
                    0,
                ))
            ]
        )
    }

    fn create_trash_agent() -> Agent {
        // Create the empty agent
        let mut agent = Agent::new(Item::new(
            Some(State::default()),
            Some(Shape::default()),
            Vec::new(),
            Vec::new(),
        ));
        // Add the Hormons to the agent. Only Hunger for now
        agent.add_hormons(vec![
            Hormon {
                name: String::from("Joy"),
                data: HormonData::new(5.0, StaticChange::Linear(-0.05))
            },
            Hormon {
                name: String::from("Tiredness"),
                data: HormonData::new(6.0, StaticChange::Linear(0.2))
            }
        ]);
        // Add Goals to the agent - only KeepHungerLow for now
        agent.add_goals(vec![
            Goal {
                name: String::from("BeHappy"),
                data: GoalData::new(vec![
                    Connection { hormon: String::from("Hunger"), value: 1.5 }
                ])
            },
            Goal {
                name: String::from("Explore"),
                data: GoalData::new(vec![
                    Connection { hormon: String::from("Joy"), value: 0.01}
                ])
            },
            Goal {
                name: String::from("RemainActive"),
                data: GoalData::new(vec![
                    Connection { hormon: String::from("Tiredness"), value: 0.1}
                ])
            }
        ]);
        // Add Actions to the agent - Eat and Move
        agent.add_actions(vec![
            Action::new(
                String::from("PickUp"),
                ActionData::new(
                    vec![
                        Requirement::Action(Event::new(
                            String::from("MoveTo"),
                            None,
                        ))
                    ],
                    vec![
                        Effect::new(String::from("Hunger"), Change::Absolute(-2.5))
                    ],
                    Some(2.0),
                    true,
                    0,
                )
            ),
            Action::new(
                String::from("Search"),
                ActionData::new(
                    Vec::new(),
                    Vec::new(),
                    None,
                    false,
                    0,
                )
            ),
            Action::new(
                String::from("MoveTo"),
                ActionData::new(
                    Vec::new(),
                    vec![
                        Effect::new(
                            String::from("Hunger"),
                            Change::Dynamic(DynamicChange::Position(0.02)),
                        )
                    ],
                    None,
                    true,
                    0,
                )
            )
        ]);
        agent
    }

    #[test]
    fn create_agent() {
        let mut agent = create_test_agent();
    }

    #[test]
    fn create_item() {
        let mut item = create_test_item(Point::new(0,0));
    }

    #[test]
    fn apply_absolute_effect() {
        // Precreate the String from the hormon name
        let hormon_name = String::from("Hunger");
        // Change the Hunger hormon by -2.5
        let effect = Effect::new(
            hormon_name.clone(),
            Change::Absolute(-2.5),
        );
        // Create the test agent and apply the effect
        let mut agent = create_test_agent();
        agent.apply_effects(&vec![effect]);
        // Check if the Hunger hormon level is currently 2.5
        assert_eq!(2.5, agent.get_hormon_level(&hormon_name).unwrap());
    }

    #[test]
    fn apply_relative_effect() {
        // Precreate the String from the hormon name
        let hormon_name = String::from("Hunger");
        // Change the Hormon by 20%
        let effect = Effect::new(
            hormon_name.clone(),
            Change::Relative(-0.2)
        );
        // Create the agent and apply the effect
        let mut agent = create_test_agent();
        agent.apply_effects(&vec![effect]);
        // Check if the level is 4 (20% of 5 is 1)
        assert_eq!(4.0, agent.get_hormon_level(&hormon_name).unwrap());
    }

    #[test]
    fn calculate_goal_importance() {
        // Create the test agent and update goal priorities
        let mut agent = create_test_agent();
        agent.eval_goals();
        assert_eq!(7.5, agent.get_goal_importance(&String::from("KeepHungerLow")).unwrap());
    }

    #[test]
    fn select_best_goal() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Sort the goals
        let selected_goal = agent.eval_goals().unwrap();
        // The highest priority should be KeepHungerLow
        assert_eq!(String::from("KeepHungerLow"), selected_goal);
    }

    #[test]
    fn alter_select_best_goal() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Apply an effect decresing the the Hunger hormon
        let effect = Effect::new(String::from("Hunger"), Change::Absolute(-10.0));
        agent.apply_effects(&vec![effect]);
        // Sort the goals
        let selected_goal = agent.eval_goals().unwrap();
        // The highest priority should be Explore
        assert_eq!(String::from("Explore"), selected_goal);
    }

    #[test]
    fn get_item_when_live() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Define item position
        let pos_a = Point::new(0,0);
        let pos_b = Point::new(1,2);
        // Create the item
        let item_a = create_test_item(pos_a);
        let item_b = create_test_item(pos_b);
        // Add item to the agent
        let id_a = agent.insert_item(item_a, None).unwrap();
        agent.insert_item(item_b, None).unwrap();
        // Return with item
        let item_a = agent.get_item(&id_a).unwrap();
        // Check if the correct one is returned
        assert_eq!(pos_a, item_a.state.unwrap().position);
        assert_eq!(create_test_item(pos_a).accepts.len(), item_a.accepts.len())
    }

    #[test]
    fn update_item() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Create positions
        let pos_a = Point::new(0,0);
        let pos_b = Point::new(1,2);
        let pos_c = Point::new(4,3);
        // Create the items
        let item_a = create_test_item(pos_a);
        let item_b = create_test_item(pos_b);
        // Insert items
        let id_a = agent.insert_item(item_a, None).unwrap();
        agent.insert_item(item_b, None);
        // Update item A
        let update_item = Item::new(
            Some(State::new(pos_c, Vector::new(0,0))),
            None,
            Vec::new(),
            Vec::new(),
        );
        assert_eq!(true, agent.update_item(update_item, &id_a));
        // Check if item got updated
        assert_eq!(pos_c, agent.get_item(&id_a).unwrap().state.unwrap().position);
        // Check if the other fields are ok
        assert_eq!(create_test_item(pos_a).accepts.len(), agent.get_item(&id_a).unwrap().accepts.len());
    }

    #[test]
    fn get_item_when_dead() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Create positions
        let pos_a = Point::new(0,0);
        let pos_b = Point::new(1,2);
        // Create items
        let item_a = create_test_item(pos_a);
        let item_b = create_test_item(pos_b);
        // Add items
        agent.insert_item(item_a, None);
        let id_b = agent.insert_item(item_b, None).unwrap();
        // Release the items
        agent.release_item(&id_b);
        // Get item
        let item_b = agent.get_item(&id_b).unwrap();
        // Compare
        assert_eq!(pos_b, item_b.state.unwrap().position);
    }

    #[test]
    fn update_item_after_release() {
        // Create test agent
        let mut agent = create_test_agent();
        // Create points
        let pos_a = Point::new(0,0);
        let pos_b = Point::new(1,2);
        let pos_c = Point::new(4,3);
        // Create items
        let item_a = create_test_item(pos_a);
        let item_b = create_test_item(pos_b);
        let update_item = create_test_item(pos_c);
        // Add items
        let id_a = agent.insert_item(item_a, None).unwrap();
        agent.insert_item(item_b, None);
        // Release item
        agent.release_item(&id_a);
        // Inser item with id_a
        agent.insert_item(update_item, Some(&id_a));
        // Get and compare
        let item_c = agent.get_item(&id_a).unwrap();
        assert_eq!(pos_c, item_c.state.unwrap().position);
        assert_eq!(create_test_item(pos_c).accepts.len(), item_c.accepts.len())
    }

    #[test]
    fn available_actions() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Get the list of actions currently available
        let available = agent.list_available_events();
        assert_eq!(1, available.len());
        // Add item
        let item = create_test_item(Point::new(0,0));
        let id = agent.insert_item(item, None).unwrap();
        // Get the available actions now
        let available = agent.list_available_events();
        assert_eq!(4, available.len());
        // Release the item, then get the action could - not should change
        agent.release_item(&id);
        let available = agent.list_available_events();
        assert_eq!(4, available.len())
    }

    #[test]
    fn select_best_action_combination() {
        // Create the test agent
        let mut agent = create_test_agent();
        // Create two items - a closer one, and another further away
        let item_close = create_test_item(Point::new(1,1));
        let item_far = create_test_item(Point::new(20, 10));
        // Add the items
        let id_close = agent.insert_item(item_close, None).unwrap();
        let id_far = agent.insert_item(item_far, None).unwrap();
        // Get available events
        let mut events = agent.list_available_events_sorted(&String::from("KeepHungerLow"));
        for event in &events {
            println!("{}: {}", event.action_name, event.profit.unwrap());
        }
        //assert_eq!(Target::Item(id_close), *events[0].target.as_ref().unwrap());
    }

    #[test]
    fn requirements_of_effect() {
        // Create test agent
        let mut agent = create_test_agent();
        // Create test item
        let item = create_test_item(Point::new(5,5));
        // Add item
        let id = agent.insert_item(item, None).unwrap();
        // Get all available events
        let events = agent.list_available_events_sorted(&String::from("KeepHungerLow"));
        // Get the requirements of the best event
        let requirements = agent.get_event_requirement(&events[0]);
        assert_eq!(1, requirements.len());
        if let Requirement::Action(event) = &requirements[0] {
            //pass
            if event.action_name != String::from("MoveTo") {
                assert!(false);
            }
        } else { assert!(false) }
    }

    #[test]
    fn resolve_event() {
        // Create test agent
        let mut agent = create_test_agent();
        // Create test item
        let item = create_test_item(Point::new(5,5));
        // Add item
        let id = agent.insert_item(item, None).unwrap();
        // Get all available events
        let events = agent.list_available_events_sorted(&String::from("KeepHungerLow"));
        // Get the requirements of the best event
        let requirements = agent.get_event_requirement(&events[0]);
        // Resolve requirements
        let child_events = agent.resolve_requirement(&requirements, &events[0].target);
        assert_eq!(1, child_events.len());
        assert_eq!(1, child_events[0].len());
    }

    #[test]
    fn event_tree() {
        // Create test agent
        let mut agent = create_test_agent();
        // Create test item
        let item_a = create_test_item(Point::new(5,5));
        let item_b = create_test_item(Point::new(15, 41));
        let id_a = agent.insert_item(item_a, None).unwrap();
        let id_b = agent.insert_item(item_b, None).unwrap();
        let tree = agent.generate_event_tree();
        agent.add_item_to_inventory(create_test_item(Point::new(0,0)));
        agent.generate_event_tree();
    }

    #[test]
    fn select_event() {
        // Create the agent
        let mut agent = create_test_agent();
        // Create test item
        let item = create_test_item(Point::new(5,5));
        let id_out = agent.insert_item(item, None);
        let item = create_test_item(Point::new(0,0));
        agent.add_item_to_inventory(item);
        // Find the most optimal event branch and set the next event
        agent.select_and_set_event();
        match agent.status {
            Status::WaitingEvent(event) => {
                assert_eq!(String::from("Eat"), event.action_name);
                assert_eq!(Target::Inventory(), event.target.unwrap());
            }
            _ => assert!(false),
        }
    }
}