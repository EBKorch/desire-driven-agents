use geom2d::{
    Point,
    Vector,
    LineSegment,
};

#[derive(Clone)]
pub struct State {
    pub position: Point,
    pub velocity: Vector,
}

impl State {
    pub fn new(position: Point, velocity: Vector) -> Self {
        State { position, velocity }
    }
}

impl Default for State {
    fn default() -> Self {
        State {
            position: Point::new(0, 0),
            velocity: Vector::new(0, 0),
        }
    }
}

#[derive(Clone)]
pub enum Shape {
    Undefined,
    Area(Vec<LineSegment>),
    Body(Vec<LineSegment>)
}

impl Default for Shape  {
    fn default() -> Self {
        let mut segments = Vec::new();
        segments.push(LineSegment::new(-0.1, -0.1, -0.1, 0.1));
        segments.push(LineSegment::new(-0.1, 0.1, 0.1, 0.1));
        segments.push(LineSegment::new(0.1, 0.1, 0.1, -0.1));
        segments.push(LineSegment::new(0.1, -0.1, -0.1, 0.1));

        Shape::Body(segments)
    }
}

#[derive(Clone)]
pub struct PhysicalRepr {
    state: State,
    shape: Shape,
}