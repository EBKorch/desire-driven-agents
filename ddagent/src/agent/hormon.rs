use super::{
    effect::{ Change, DynamicChange },
    item::Item,
};

#[derive(Clone)]
pub struct Hormon {
    pub name: String,
    pub data: HormonData
}

#[derive(Clone)]
pub struct HormonData {
    level: f64,
    static_change: StaticChange,
    base: f64,
}

impl HormonData {
    pub fn new(level: f64, static_change: StaticChange) -> Self {
        let base = HormonData::level_to_base(&static_change, &level);
        HormonData { level, static_change, base }
    }

    pub fn get_level(&self) -> f64 {
        self.level
    }

    pub fn apply_change(&mut self, change: &Change) {
        match change {
            Change::Absolute(value) => self.level += value,
            Change::Relative(value) => self.level += self.level*value,
            Change::Set(value) => self.level = *value,
            Change::Dynamic(dynamic_change) => unreachable!(),
        }
    }

    pub fn try_change(&self, change: &Change) -> Self {
        let mut hormon = self.clone();
        hormon.apply_change(change);
        hormon
    }

    fn level_to_base(static_change: &StaticChange, level: &f64) -> f64 {
        match static_change {
            StaticChange::Linear(value) => level/value,
            StaticChange::Disabled => *level,
        }
    }

    fn base_to_level(static_change: &StaticChange, base: &f64) -> f64 {
        match static_change {
            StaticChange::Linear(value) => base*value,
            StaticChange::Disabled => *base,
        }
    }

    /// Update the hormon level after `elpased_time`
    pub fn selfchange(&mut self, elapsed_time: &f64) {
        // Calculate the base value from current level, then step the base with the
        // elapsed time. This way it will not matter how big the time jump is, we
        // will always get correct value for the level.
        match self.static_change {
            StaticChange::Disabled => (),
            _ => {
                self.base = HormonData::level_to_base(&self.static_change, &self.level) + elapsed_time;
                self.level = HormonData::base_to_level(&self.static_change, &self.base);
            }
        }
    }

    /// Create a new hormon with level after `elapsed_time`
    pub fn try_selfchange(&self, elapsed_time: &f64) -> Self {
        let mut hormon = self.clone();
        hormon.selfchange(elapsed_time);
        hormon
    }
}

impl Default for HormonData {
    fn default() -> Self {
        let static_change = StaticChange::Linear(1.0);
        HormonData::new(0.0, static_change)
    }
}

#[derive(Clone)]
pub enum StaticChange {
    Disabled,
    Linear(f64),
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn static_change_disabled() {
        let level = 1.0;
        let mut hormon = HormonData::new(level, StaticChange::Disabled);
        hormon.selfchange(&10.0);
        assert_eq!(level, hormon.get_level());
    }

    #[test]
    fn static_change_linear() {
        let level = 1.0;
        let m = 2.0;
        let dt = 1.5;
        let mut hormon = HormonData::new(level, StaticChange::Linear(m));
        hormon.selfchange(&dt);
        assert_eq!(level+m*dt, hormon.get_level());
    }
}
